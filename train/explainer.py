"""https://github.com/histocartography/histocartography/blob/main/histocartography/interpretability/grad_cam.py"""
from typing import List, Optional, Tuple, Union
import dgl
import numpy as np
import torch
import torch.nn.functional as F
import pandas as pd

EPS = 10e-7
class BaseCAM(object):
    def __init__(self, model: torch.nn.Module, conv_layers: List[str]) -> None:
        """
        BaseCAM constructor.
        Args:
            model (torch.nn.Module): Input model.
            conv_layer (List[str]): List of tensor names to compute activations on.
        """
        self.model = model
        self.forward_hook = list()
        self.backward_hook = list()
        self.hook_handles = list()

        # Forward hooks
        for conv_layer in conv_layers:
            if not hasattr(model, conv_layer):
                raise ValueError(
                    f"Unable to find submodule {conv_layers} in the model")
            self.hook_handles.append(
                self.model._modules.get(conv_layer).register_forward_hook(
                    self._set_forward_hook))
        # Backward hooks
        for conv_layer in conv_layers:
            self.hook_handles.append(
                self.model._modules.get(conv_layer).register_backward_hook(
                    self._set_backward_hook))
        # Enable hooks
        self._hooks_enabled = True
        # Should ReLU be used before normalization
        self._relu = True
        # Model output is used by the extractor
        self._score_used = False

    def _set_forward_hook(self, module, input, output):
        """Hook activations (forward hook)."""
        if self._hooks_enabled:
            self.forward_hook.append(output.data)

    def _set_backward_hook(self, module, input, output):
        """Hook gradient (backward hook)."""
        if self._hooks_enabled:
            self.backward_hook.append(output[0].data)

    def clear_hooks(self):
        """Clear model hooks."""
        for handle in self.hook_handles:
            handle.remove()

    @staticmethod
    def _normalize(cams):
        """CAM normalization."""
        cams -= cams.min(0).values
        cams /= cams.max(0).values + EPS
        return cams

    def _get_weights(self, class_idx, scores=None):
        raise NotImplementedError

    def _precheck(self, class_idx, scores):
        """Check for invalid computation cases"""

        # Check that forward has already occurred
        if not self.forward_hook:
            raise AssertionError(
                "Inputs need to be forwarded in the model for the conv features to be hooked"
            )

        # Check class_idx value
        if class_idx < 0:
            raise ValueError("Incorrect `class_idx` argument value")

        # Check scores arg
        if self._score_used and not isinstance(scores, torch.Tensor):
            raise ValueError(
                "Model output scores is required to be passed to compute CAMs"
            )

    def __call__(self, class_idx, scores=None, normalized=True):
        """
        Compute CAM for a specified class
        Args:
            class_idx (int): Output class index of the target class whose CAM will be computed.
            scores (torch.Tensor[1, K], optional): Forward output scores of the hooked model, ie logits.
            normalized (bool, optional): Whether the CAM should be normalized.
        Returns:
            torch.Tensor[M, N]: Class activation map of hooked conv layer.
        """

        # Integrity check
        self._precheck(class_idx, scores)

        # Get map weight
        weights = self._get_weights(class_idx, scores)
        is_cuda = weights.is_cuda

        # Perform the weighted combination to get the CAM
        forwards = torch.stack(self.forward_hook, dim=2)
        num_nodes = forwards.squeeze(0).shape[0]
        batch_cams = (
            weights.unsqueeze(0).repeat(num_nodes, 1, 1) * forwards.squeeze(0)
        ).sum(dim=1)

        if is_cuda:
            batch_cams = batch_cams.cuda()

        if self._relu:
            batch_cams = F.relu(batch_cams, inplace=True)

        # Normalize the CAM
        if normalized:
            batch_cams = self._normalize(batch_cams)

        # Average out the different weights of the layers
        batch_cams = batch_cams.mean(dim=1)

        return batch_cams

    def __repr__(self):
        return f"{self.__class__.__name__}()"

    def _backprop(self, scores, class_idx):
        """Backpropagate the loss for a specific output class"""

        if self.forward_hook is None:
            raise TypeError(
                "Apply forward path before calling backward hook."
            )

        # Backpropagate to get the gradients on the hooked layer
        loss = scores[:, class_idx].sum()
        self.model.zero_grad()
        loss.backward(retain_graph=True)


class GradCAM(BaseCAM):
    """
        Class activation map extraction as in `"Grad-CAM: Visual Explanations from Deep Networks
        via Gradient-based Localization" <https://arxiv.org/pdf/1610.02391.pdf>`_.
    """

    def _get_weights(self, class_idx, scores):
        """Computes the weight coefficients of the hooked activation maps"""
        # Backpropagate
        self._backprop(scores, class_idx)
        grads = torch.stack(list(reversed(self.backward_hook)), dim=2)
        return grads.mean(axis=0)

    def __call__(self, *args, **kwargs):
        self.backward_hook = list()
        return super().__call__(*args, **kwargs)


class GradCAMpp(BaseCAM):
    """
    Class activation map extraction as in `"Grad-CAM++: Improved Visual Explanations for
    Deep Convolutional Networks" <https://arxiv.org/pdf/1710.11063.pdf>`_.
    """

    def _get_weights(self, class_idx, scores):
        """Computes the weight coefficients of the hooked activation maps"""

        # Backpropagate
        self._backprop(scores, class_idx)

        self.backward_hook = list(reversed(self.backward_hook))

        # Compute alpha
        grad_2 = [f.pow(2) for f in self.backward_hook]
        grad_3 = [f.pow(3) for f in self.backward_hook]
        alpha = [g2 / (
            2 * g2 + (g3 * a).sum(axis=(0), keepdims=True) + EPS
        ) for g2, g3, a in zip(grad_2, grad_3, self.forward_hook)
        ]

        weights = [
            a.squeeze_(0).mul_(
                torch.relu(
                    g.squeeze(0))).sum(
                axis=(0)) for a, g in zip(
                    alpha, self.backward_hook)]
        weights = torch.stack(weights, dim=1)

        return weights

    def __call__(self, *args, **kwargs):
        self.backward_hook = list()
        return super().__call__(*args, **kwargs)
    

""""https://github.com/ndey96/GCNN-Explainability/blob/master/explain.py"""   
import torch
import torch.nn.functional as F
import numpy as np
import matplotlib.pyplot as plt
from rdkit.Chem import rdDepictor
from rdkit.Chem import Draw
from rdkit.Chem.Draw import rdMolDraw2D
import matplotlib
import matplotlib.cm as cm
from skimage.io import imread
from cairosvg import svg2png, svg2ps
import os
from torch_geometric.data import DataLoader
import pandas as pd
from rdkit import Chem
from rdkit.Chem import AllChem
from rdkit.Chem.Draw import SimilarityMaps
from sklearn.preprocessing import MinMaxScaler
from tqdm import tqdm
import random
import wandb
import PIL
from typing import *
# from torch_geometric.utils import unbatch, unbatch_edge_index

def img_for_mol(mol, atom_weights=[], bond_weights: Union[None, List]=[], start_idx: int=0, edge_index: torch.LongTensor=None, use_custom_draw: bool=True, new_edge_index: torch.LongTensor=None):
    # print(atom_weights)
    highlight_kwargs = {}
    
    if len(atom_weights) > 0:
        norm = matplotlib.colors.Normalize(vmin=-1, vmax=1)
        cmap = cm.get_cmap('bwr')
        plt_colors = cm.ScalarMappable(norm=norm, cmap=cmap)
        atom_colors = {
            i: plt_colors.to_rgba(atom_weights[i]) for i in range(len(atom_weights))
        } #DICT: {atm_num: color_val}
        
        
        bonds = []
        for bond in mol.GetBonds():
            bonds.append((bond.GetBeginAtomIdx(), bond.GetEndAtomIdx()))
        bonds_zip = list(zip(*bonds)) #2, Edges
        bonds_zip = np.array(bonds_zip).T #edges, 2
        # print(bonds_zip, edge_index.t())
        
        if new_edge_index != None:
            # print(bonds_zip, new_edge_index)
            bw_list = []
            for bond in bonds_zip:
                idx_of_geomedge = (bond == new_edge_index.t().detach().cpu().numpy()).all(axis=-1).tolist().index(True)
                w = bond_weights[idx_of_geomedge]  
                bw_list.append(w) #num_real_bonds
            bond_colors = {
                i: plt_colors.to_rgba(bw_list[i]) for i in range(len(bw_list))
            }
        else:
            bw_list = []
            bond_colors = {}
                # all_bonds.append( (bond == new_edge_index).all(axis=-1) ) 
            # all_bonds = np.array(all_bonds).T.any(axis=-1) #(real_bonds, torch_geom_bonds) - > (torch_geom_bonds, real_bonds) -> Boolean array of (torch_geom_bonds, )


        """
        # np.isin(edge_index.detach().cpu().numpy().T, bonds_zip)

        if not isinstance(bond_weights, type(None)):
            bond_colors = {
                i: plt_colors.to_rgba(bond_weights[i]) for i in range(len(bond_weights))
            }
        else:
            bond_weights = []
            bond_colors = {}
        """
        if new_edge_index != None:
            highlight_kwargs = {
                'highlightAtoms': [],
                'highlightAtomColors': {},
                'highlightBonds': list(range(len(bw_list))),
                'highlightBondColors': bond_colors
            }
        else:
            highlight_kwargs = {
                'highlightAtoms': list(range(len(atom_weights))),
                'highlightAtomColors': atom_colors,
                'highlightBonds': list(range(len(bw_list))),
                'highlightBondColors': bond_colors
            }

        # print(highlight_kwargs)
    # print(bond_weights, mol.GetNumBonds())
    

    #########################
    #METHOD 1 for DRAWING MOL (DrawMolecule)
    #########################
    if use_custom_draw:
        rdDepictor.Compute2DCoords(mol)
        drawer = rdMolDraw2D.MolDraw2DSVG(280, 280)
        drawer.drawOptions().addStereoAnnotation=True
        drawer.atomHighlightsAreCircles = True
        drawer.fillHighlights=False
        drawer.SetFontSize(1)
        mol = rdMolDraw2D.PrepareMolForDrawing(mol)
        drawer.DrawMolecule(mol, **highlight_kwargs)
                            # highlightAtoms=list(range(len(atom_weights))),
                            # highlightBonds=[],
                            # highlightAtomColors=atom_colors)
        # PrepareAndDrawMolecule #https://www.rdkit.org/docs/GettingStartedInPython.html?highlight=maccs#:~:text=%2C%20500)-,%3E%3E%3E%20rdMolDraw2D.PrepareAndDrawMolecule(d%2C%20mol%2C%20highlightAtoms%3Dhit_ats%2C,-...%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20highlightAtomColors%3D
        drawer.FinishDrawing()
        svg = drawer.GetDrawingText()
        svg = svg.replace('svg:', '')
        svg2png(bytestring=svg, write_to='tmp.png', dpi=100)
        img = imread('tmp.png')
        os.remove('tmp.png')
    #########################

    #########################
    #METHOD 2 for DRAWING MOL (SimilarityMaps.GetSimilarityMapFromWeights)
    #########################
    else:
        import io
        from rdkit.Chem.Draw import SimilarityMaps
        drawer = rdMolDraw2D.MolDraw2DSVG(280, 280)
        # drawer.SetFontSize(1)
        # drawer = Draw.MolDraw2DCairo(400, 400)
        # print(list(atom_colors.values()))
        img = SimilarityMaps.GetSimilarityMapFromWeights(mol, atom_weights) #http://rdkit.blogspot.com/2020/01/similarity-maps-with-new-drawing-code.html#:~:text=SimilarityMaps.GetSimilarityMapFromWeights(atorvastatin%2Clist(mean_chgs)%2Cdraw2d%3Dd)
        img.savefig("tmp.png", bbox_inches='tight')
        img = imread('tmp.png')
        os.remove('tmp.png')
        # def show_png(data):
        #     from PIL import Image
        #     bio = io.BytesIO(data)
        #     img = Image.open(bio)
        #     img.save("tmp.png") #PIL.Image to png
        #     img = imread('tmp.png')
        #     os.remove('tmp.png')
        #     return img
        drawer.FinishDrawing()
        # img = show_png(drawer.GetDrawingText())
    # #########################

    #BELOW GRID Images:
    # https://www.rdkit.org/docs/Cookbook.html#:~:text=Draw.MolsToGridImage(mMols%2Clegends%3D%5BmurckoHash%20for%20murckoHash%20in%20murckoHashList%5D%2C
    return img

def plot_explanations(model: torch.nn.Module, gb_model: torch.nn.Module, batch: torch.LongTensor, batch_index: int, smiles: str=None, end_idx: int=None, edge_index: torch.LongTensor=None, return_metadata=False):
    
    if isinstance(smiles, list):
        assert len(smiles) == 1, "Only one SMILES at a time!"
        smiles = smiles[0] #take out a string from list
    elif isinstance(smiles, str):
        pass #if already a string, pass!
    
    assert np.any([p.grad != None for p in model.parameters()]), "backpropagation is not done!" #Must be true for backprop!
    
    mol = Chem.MolFromSmiles(smiles)
    mol = AllChem.AddHs(mol) 
    selected_batch_nodes = (batch == batch_index)
    num_atoms = mol.GetNumAtoms()
    start_idx = end_idx - num_atoms
    selected_batch_edges = torch.logical_and((edge_index[0] >= start_idx), (edge_index[0] < end_idx))
    new_edge_index = (edge_index[:, selected_batch_edges] - start_idx).long() #2, edges
    print(num_atoms)

    use_custom_draw = True
    fig, axes = plt.subplots(3, 2, figsize=(12, 8))
    axes[0][0].imshow(img_for_mol(mol))
    axes[0][0].set_title(smiles)

    if hasattr(model, "attention_weight"): 
        assert model.attention_weight != None, "model must have attention weights"
        input_attentions = model.attention_weight.detach().cpu().numpy()  #[selected_batch_edges].detach().cpu().numpy() 
        scaled_input_attentions = MinMaxScaler(feature_range=(-1,1)).fit_transform(np.array(input_attentions).reshape(-1, 1)).reshape(-1, )
    else:
        scaled_input_attentions = None #None or Tensor!

    axes[0][1].set_title('Saliency Map')
    assert model.embedded_grad[0][selected_batch_nodes, :].size(0) == mol.GetNumAtoms(), "Number of nodes embedded mismatched with actual atom numbers!"
    input_grads = model.embedded_grad[0][selected_batch_nodes, :].view(mol.GetNumAtoms(), -1)
    saliency_map_weights = saliency_map(input_grads) #self.embedding; np
    scaled_saliency_map_weights = MinMaxScaler(feature_range=(-1,1)).fit_transform(np.array(saliency_map_weights).reshape(-1, 1)).reshape(-1, )
    axes[0][1].imshow(img_for_mol(mol, atom_weights=scaled_saliency_map_weights, bond_weights=None, start_idx=start_idx, edge_index=edge_index[:,selected_batch_edges], use_custom_draw=use_custom_draw))
   
    assert model.final_conv_acts[selected_batch_nodes, :].size(0) == mol.GetNumAtoms() and model.final_conv_grads[selected_batch_nodes, :].size(0) == mol.GetNumAtoms() , "Number of nodes embedded mismatched with actual atom numbers!"
    final_conv_acts = model.final_conv_acts[selected_batch_nodes, :].view(mol.GetNumAtoms(), -1)
    final_conv_grads = model.final_conv_grads[selected_batch_nodes, :].view(mol.GetNumAtoms(), -1)
    
    # axes[1][0].set_title('Grad-CAM')
    # axes[1][0].set_title('Grad-CAM')
    # grad_cam_weights = grad_cam(final_conv_acts, final_conv_grads) #; np
    # scaled_grad_cam_weights = MinMaxScaler(feature_range=(0,1)).fit_transform(np.array(grad_cam_weights).reshape(-1, 1)).reshape(-1, )
    # axes[1][0].imshow(img_for_mol(mol, atom_weights=scaled_grad_cam_weights, bond_weights=scaled_input_attentions, start_idx=start_idx, edge_index=edge_index))
    # fig = SimilarityMaps.GetSimilarityMapFromWeights(mol, scaled_grad_cam_weights.tolist(), colorMap='jet', contourLines=10, draw2d=Draw.MolDraw2DCairo(2, 2)) #draw2d=Draw.MolDraw2DCairo(400, 400)
    # ax = fig.gca()
    # axes[1][0] = ax
    
    axes[1][0].set_title('Grad-CAM ++')
    grad_campp_weights = gradcampp(final_conv_acts, final_conv_grads) #np
    scaled_grad_campp_weights = MinMaxScaler(feature_range=(-1,1)).fit_transform(np.array(grad_campp_weights).reshape(-1, 1)).reshape(-1, )
    axes[1][0].imshow(img_for_mol(mol, atom_weights=scaled_grad_campp_weights, bond_weights=None, start_idx=start_idx, edge_index=edge_index, use_custom_draw=use_custom_draw))
    
    # axes[1][1].set_title('UGrad-CAM')
    # ugrad_cam_weights = ugrad_cam(final_conv_acts, final_conv_grads) #np
    # scaled_ugrad_cam_weights = MinMaxScaler(feature_range=(-1,1)).fit_transform(np.array(ugrad_cam_weights).reshape(-1, 1)).reshape(-1, )
    # axes[1][1].imshow(img_for_mol(mol, atom_weights=scaled_ugrad_cam_weights, bond_weights=scaled_input_attentions, start_idx=start_idx, edge_index=edge_index, use_custom_draw=use_custom_draw))

    axes[1][1].set_title('Guided Backpropagation')
    assert gb_model.model.embedded_grad[0][selected_batch_nodes, :].size(0) == mol.GetNumAtoms(), "Number of nodes embedded mismatched with actual atom numbers!"
    input_grads = gb_model.model.embedded_grad[0][selected_batch_nodes, :].view(mol.GetNumAtoms(), -1)
    gb_weights = guided_backprop(input_grads) #np
    scaled_gb_weights = MinMaxScaler(feature_range=(-1,1)).fit_transform(np.array(gb_weights).reshape(-1, 1)).reshape(-1, )
    axes[1][1].imshow(img_for_mol(mol, atom_weights=scaled_gb_weights, bond_weights=None, start_idx=start_idx, edge_index=edge_index, use_custom_draw=use_custom_draw))

    axes[2][0].set_title('Guided GradCAM ++')
    assert ("scaled_grad_campp_weights" in vars()) and ("scaled_gb_weights" in vars()), "Both scaled GradCAM++ and GB should exist!"
    scaled_guided_gcam_weights = scaled_grad_campp_weights * scaled_gb_weights
    axes[2][0].imshow(img_for_mol(mol, atom_weights=scaled_guided_gcam_weights, bond_weights=None, start_idx=start_idx, edge_index=edge_index, use_custom_draw=use_custom_draw))

    axes[2][1].set_title('Bond Weights') #Use saliency atom mapping for placeholder
    axes[2][1].imshow(img_for_mol(mol, atom_weights=saliency_map_weights, bond_weights=scaled_input_attentions, start_idx=start_idx, edge_index=edge_index, use_custom_draw=use_custom_draw, new_edge_index=new_edge_index))

    # weights = model.last.weight.detach().cpu().numpy() #(1,100)
    # cam_weights = cam(weights, final_conv_acts)
    # scaled_cam_weights = MinMaxScaler(feature_range=(-1,1)).fit_transform(np.array(cam_weights).reshape(-1, 1)).reshape(-1, )


    fig.set_tight_layout(True)
    norm = matplotlib.colors.Normalize(vmin=-1, vmax=1)
    cmap = cm.get_cmap('bwr')
    im = cm.ScalarMappable(norm=norm, cmap=cmap)
    fig.colorbar(im, cax=plt.axes([0.85, 0.1, 0.075, 0.8]))

#     plt.savefig(f'explanations/{mol_num}.png')
#     plt.close('all')
    #plt to PIL: https://stackoverflow.com/questions/57316491/how-to-convert-matplotlib-figure-to-pil-image-object-without-saving-image#:~:text=%22%22%22Convert%20a%20Matplotlib%20figure%20to%20a%20PIL%20Image%20and%20return%20it%22%22%22%0A%20%20%20%20import%20io%0A%20%20%20%20buf%20%3D%20io.BytesIO()%0A%20%20%20%20fig.savefig(buf)%0A%20%20%20%20buf.seek(0)%0A%20%20%20%20img%20%3D%20Image.open(buf)%0A%20%20%20%20return%20img
    import io
    buf = io.BytesIO()
    fig.savefig(buf)
    buf.seek(0)
    img = PIL.Image.open(buf)
    if not return_metadata:
        return {"smiles":smiles, "figure": wandb.Image(img)}
    else: 
        return {"saliency": scaled_saliency_map_weights, "ugradcam": ugrad_cam_weights}


def saliency_map(input_grads):
    # print('saliency_map')
#     node_saliency_map = []
#     for n in range(input_grads.shape[0]): # nth node
#         node_grads = input_grads[n,:]
#         node_saliency = torch.norm(F.relu(node_grads)).item()
#         node_saliency_map.append(node_saliency)
#     return node_saliency_map
    node_saliency_map = torch.norm(F.relu(input_grads), dim=-1) #nodes
    return node_saliency_map.detach().cpu().numpy() #np

def cam(weights, final_conv_acts):
    # https://medium.com/intelligentmachines/implementation-of-class-activation-map-cam-with-pytorch-c32f7e414923
    node_heat_map = final_conv_acts @ weights.T #(num_nodes, 100) @ (100, 1)
    return node_heat_map

def grad_cam(final_conv_acts, final_conv_grads):
    # print('grad_cam')
#     node_heat_map = []
#     alphas = torch.mean(final_conv_grads, axis=0) # mean gradient for each feature (512x1)
#     for n in range(final_conv_acts.shape[0]): # nth node
#         node_heat = F.relu(alphas @ final_conv_acts[n]).item()
#         node_heat_map.append(node_heat)
#     return node_heat_map
    alphas = final_conv_grads.mean(dim=0) #(dim,)
    node_heat_map = F.relu(alphas * final_conv_acts).sum(dim=-1) #(1,dim) * (nodes, dim) -> (nodes,)
    return node_heat_map.detach().cpu().numpy() #np

def ugrad_cam(final_conv_acts, final_conv_grads):
    # print('new_grad_cam')
#     node_heat_map = []
#     alphas = torch.mean(final_conv_grads, axis=0) # mean gradient for each feature (512x1)
#     for n in range(final_conv_acts.shape[0]): # nth node
#         node_heat = (alphas @ final_conv_acts[n]).item()
#         node_heat_map.append(node_heat)
    alphas = final_conv_grads.mean(dim=0) #(dim,)
    node_heat_map = F.relu(alphas * final_conv_acts).sum(dim=-1) #(1,dim) * (nodes, dim) -> (nodes,)
    node_heat_map = node_heat_map.detach().cpu().numpy().reshape(-1, 1)
    pos_node_heat_map = MinMaxScaler(feature_range=(0,1)).fit_transform(node_heat_map*(node_heat_map >= 0)).reshape(-1,)
    neg_node_heat_map = MinMaxScaler(feature_range=(-1,0)).fit_transform(node_heat_map*(node_heat_map < 0)).reshape(-1,)
    return pos_node_heat_map + neg_node_heat_map #np

def gradcampp(final_conv_acts, final_conv_grads):
    grads_power_2 = final_conv_grads**2 #(nodes, dim)
    grads_power_3 = grads_power_2 * final_conv_grads #(nodes, dim)
    sum_activations = final_conv_acts.sum(dim=(0), keepdim=True) #(1,dim)
    eps = 0.000001
    aij = grads_power_2 / (2 * grads_power_2 +
                            sum_activations * grads_power_3 + eps) #(nodes, dim)
    aij = torch.where(final_conv_grads != final_conv_grads.new_tensor(0.), aij, final_conv_grads.new_tensor(0.)) #Non-zeros #(nodes, dim)
    weights = torch.maximum(final_conv_grads, final_conv_grads.new_tensor(0.)) * aij #Only positive #(nodes, dim)
    weights = weights.sum(dim=(0), keepdim=True) #(1,dim)
    gradcampp = (final_conv_acts * weights).sum(dim=1, keepdim=False) #(nodes, dim) --> (nodes, )
    gradcampp = torch.maximum(gradcampp, final_conv_grads.new_tensor(0.)).detach().cpu().numpy() #Only positives #(nodes, )
    return gradcampp
    
def guided_backprop(input_grads):
    node_gb_map = torch.norm(input_grads, dim=-1) #nodes
    return node_gb_map.detach().cpu().numpy() #np

def excitation_backprop():
    pass
    #https://github.com/greydanus/excitationbp/blob/master/excitationbp/utils.py
