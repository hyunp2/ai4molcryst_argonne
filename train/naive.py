import os, abc, sys
import pathlib
root = pathlib.Path(__file__).parent.parent #gpuHack directory
#sys.path.append(root) #add root directory
os.chdir(root)
import pdb
import glob
import copy
import wandb
import h5py
import random
import string
import argparse
import pandas as pd
import numpy as np
from scipy.special import softmax
from PIL import Image
from typing import *
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from curtsies import fmtfuncs as cf
import scipy.stats
import torchmetrics
import yaml
from tqdm import tqdm

from fast_ml.model_development import train_valid_test_split

from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score, accuracy_score, precision_score, recall_score
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score

import torchvision.transforms as transforms
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
import torch.nn.functional as F
import torch.optim as optim
import torch.nn as nn
import torch
import torchvision

from transformers import BertTokenizerFast as BertTokenizer, BertModel, AdamW, get_linear_schedule_with_warmup

from schnet import schnet
from physnet import physnet
from alignn import alignn
from torchmdnet import torchmdnet
from dimenet import dimenet
from crystals import cgcnn
from crystals import alignn as calignn
from crystals import physnet as cphysnet
from crystals import schnet as cschnet
from crystals import torchmdnet as ctorchmdnet
from config_pub import *

import captum
  
from train.dist_utils import to_cuda, get_local_rank, init_distributed, seed_everything, \
    using_tensor_cores, increase_l2_fetch_granularity, WandbLogger
from torch.utils.data import DataLoader, DistributedSampler, Dataset
from train.training_functions_pub import train as train_molecule, save_state
from crystals.training_functions_pub import train as train_crystal
from train.dataloader import DataModuleEdge, DataModuleOthers
from crystals.dataloader import DataModuleCrystal
from train.loss_utils_pub import get_loss_func, get_loss_func_crystal

from main_pub import get_parser

""""0. DEFINE MODEL CONFIGS"""
#Called from "config_pub.py"
opt = get_parser()

""""1. DEFINE FUNCTION TO MEASURE
When using Ray, automatically DDP!"""

def get_run(opt: argparse.ArgumentParser = None, cuda_number: int = 0):
    assert opt.log, "Logging must be enabled..."

    if opt.log:
        logger = wandb.init(name=opt.name,
                            project='internship',
                            entity='argonne_gnn',
                            settings=wandb.Settings(start_method="fork"),
                            id=None,
                            dir=None,
                            resume='allow',
                            anonymous='must')
                            
    os.environ["CUDA_VISIBLE_DEVICES"] = str(cuda_number)

#         device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    device = cuda_number #torch.cuda.current_device() #same as cuda_number?

    #Distributed Sampler Loader
    if opt.dataset in ["qm9edge"]:
        datamodule = DataModuleEdge(opt=opt)
    if opt.dataset in ["qm9", "md17", "ani1", "ani1x", "moleculenet"]:
        datamodule = DataModuleOthers(hparams=opt)
    if opt.dataset in ["cifdata","gandata"]:
        datamodule = DataModuleCrystal(opt=opt)
    train_loader = datamodule.train_dataloader()
    val_loader = datamodule.val_dataloader()
    test_loader = datamodule.test_dataloader()

    #Model 
    model = BACKBONES.get(opt.backbone, physnet.Physnet) #Uninitialized class
    model_kwargs = BACKBONE_KWARGS.get(opt.backbone, None) #TorchMDNet not yet!

    if opt.backbone not in ["calignn", "alignn"]: model_kwargs.update({"explain": opt.explain})  
    else: model_kwargs.explain = opt.explain #only for alignn net (due to pydantic)

    if opt.backbone in ["schnet", "physnet", "dimenet", "dimenetpp","cschnet","cphysnet","cgcnn"]:
        model = model(**model_kwargs) 
    elif opt.backbone in ["alignn","calignn"]:
        model = model(model_kwargs) #Accounting for alignn net
    elif opt.backbone in ["torchmdnet","ctorchmdnet"]:
        model = torchmdnet.create_model(model_kwargs) if opt.backbone=="torchmdnet" else ctorchmdnet.create_model(model_kwargs)
    model.to(device)

    grad_scaler = torch.cuda.amp.GradScaler(enabled=opt.amp)

    try:
#             importlib.import_module("apex.optimizers")
        from apex.optimizers import FusedAdam, FusedLAMB
    except Exception as e:
        pass

    if opt.optimizer == 'adam':
        optimizer = FusedAdam(model.parameters(), lr=opt.learning_rate, betas=(0.9, 0.999),
                            weight_decay=opt.weight_decay)
        base_optimizer = FusedAdam
        base_optimizer_arguments = dict(lr=opt.learning_rate, betas=(0.9, 0.999), weight_decay=opt.weight_decay)
    elif opt.optimizer == 'lamb':
        optimizer = FusedLAMB(model.parameters(), lr=opt.learning_rate, betas=(0.9, 0.999),
                            weight_decay=opt.weight_decay)
        base_optimizer = FusedLAMB
        base_optimizer_arguments = dict(lr=opt.learning_rate, betas=(0.9, 0.999), weight_decay=opt.weight_decay)
    elif opt.optimizer == "sgd":
        optimizer = torch.optim.SGD(model.parameters(), lr=opt.learning_rate, momentum=0.9,
                                    weight_decay=opt.weight_decay)
        base_optimizer = torch.optim.SGD
        base_optimizer_arguments = dict(lr=opt.learning_rate, momentum=0.9, weight_decay=opt.weight_decay)
    elif opt.optimizer == 'torch_adam':
        optimizer = torch.optim.Adam(model.parameters(), lr=opt.learning_rate, betas=(0.9, 0.999),
                            weight_decay=opt.weight_decay, eps=1e-8)
        base_optimizer = torch.optim.Adam
        base_optimizer_arguments = dict(lr=opt.learning_rate, betas=(0.9, 0.999), weight_decay=opt.weight_decay, eps=1e-8)
    elif opt.optimizer == 'torch_adamw':
        optimizer = torch.optim.AdamW(model.parameters(), lr=opt.learning_rate, betas=(0.9, 0.999),
                            weight_decay=opt.weight_decay, eps=1e-8)
        base_optimizer = torch.optim.AdamW
        base_optimizer_arguments = dict(lr=opt.learning_rate, betas=(0.9, 0.999), weight_decay=opt.weight_decay, eps=1e-8)
    elif opt.optimizer == 'torch_sparse_adam':
        optimizer = torch.optim.SparseAdam(model.parameters(), lr=opt.learning_rate, betas=(0.9, 0.999),
                            eps=1e-8)
        base_optimizer = torch.optim.SparseAdam
        base_optimizer_arguments = dict(lr=opt.learning_rate, betas=(0.9, 0.999), eps=1e-8)

    loss_func = get_loss_func
    total_training_steps = len(train_loader) * opt.epoches
    warmup_steps = total_training_steps // opt.warm_up_split
    scheduler: torch.optim.lr_scheduler.LambdaLR = get_linear_schedule_with_warmup(
                                            optimizer,
                                            num_warmup_steps=warmup_steps,
                                            num_training_steps=total_training_steps)
    local_rank = cuda_number #get_local_rank()
    tmetrics = torchmetrics.MeanAbsoluteError()

    try:
        if opt.crystal:
            from crystals.training_functions_pub import load_state, save_state, single_train, single_val, single_test
        else:
            from train.training_functions_pub import load_state, save_state, single_train, single_val, single_test

        pbar = tqdm(range(0, int(opt.epoches)), total=int(opt.epoches), unit='epoch', desc=f"{opt.name} with CUDA {cuda_number}",
                                disable=(opt.silent))
        opt.log = False #due to single_train, single_val's opt.log 
        best_val = 1e5
        for epoch_idx in pbar:
            dp_train_loss, dp_train_loss_mae = single_train(opt, model, train_loader, loss_func, epoch_idx, optimizer, scheduler, grad_scaler, local_rank, logger, tmetrics)
            logger.log({'DeepHyperTrainLossNaive': dp_train_loss, 'DeepHyperTrainMAELossNaive': dp_train_loss_mae, 'epoch':epoch_idx}) 
            torch.cuda.empty_cache()
            tmetrics.reset()
            # pbar.set_description(f"Processing epoch {epoch_idx} in local rank {local_rank}...")
            dp_val_loss, dp_val_loss_mae = single_val(opt, model, val_loader, loss_func, optimizer, scheduler, logger, tmetrics)
            logger.log({'DeepHyperValLossNaive': dp_val_loss, 'DeepHyperValMAELossNaive': dp_val_loss_mae, 'epoch':epoch_idx}) 
            if dp_val_loss < best_val:
                print(f"At epoch {epoch_idx}, saving a model!")
                path_and_name = os.path.join(opt.load_ckpt_path, "{}.pth".format(opt.name))
                save_state(model, optimizer, scheduler, epoch_idx, dp_val_loss, path_and_name, ignore_local_rank=True)
                best_val = dp_val_loss
            torch.cuda.empty_cache()

        print(cf.on_yellow("Training is done for suboptimal runs..."))
        opt.log = True #due to single_train, single_val's opt.log 

        tmetrics.reset()
        logger.log({'DeepHyperValLossNaive': dp_val_loss, 'DeepHyperValMAELossNaive': dp_val_loss_mae, 'epoch':epoch_idx}) 

    except Exception as e:
        print(cf.red(f"Error case happend with {e}"))
        return np.finfo(float).min   

    if opt.log:
        return logger
    else:
        return None

def submit_naive_jobs(opt):
    data = pd.read_csv(os.path.join(os.getcwd(), "grid_hp.csv"), header=0)
    hyperparam_names = list(data.columns)
    cuda_number = 0
    device_count = torch.cuda.device_count()
    opt.epoches = 30 #keep consistent with hypertuning
    f = open(os.path.join("examples", f"{opt.dataset}_{opt.task}_naive_errors.txt"), "w")

    for i in range(len(data)):
        vals = data.iloc[i, :].values #np.array
        cuda_number = 0  # i % device_count # 0 to device_count-1
        opt.name = f"{opt.dataset}_{opt.task}_{i}_naive"

        try:
            for key, val in zip(hyperparam_names, vals):
                if key == "batch_size":
                    val = int(val)
                if isinstance(val, np.bool_):
                    val = bool(val)
                opt.__dict__[key] = val
            logger = get_run(opt=opt, cuda_number=cuda_number)
            # print(logger)
            logger.finish() #
            f.write(f"SUCCESS: {opt.name}\n")
        except Exception as e:
            print(f"Error is {e}...")
            f.write(f"FAILURE: {opt.name}\n")
    f.close()

if __name__ == "__main__":
    submit_naive_jobs(opt)
