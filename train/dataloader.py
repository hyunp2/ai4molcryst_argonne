import os, abc
import pdb
import glob
import copy
import wandb
import h5py
import random
import string
import argparse
import pandas as pd
import numpy as np
from scipy.special import softmax
from PIL import Image
from typing import *
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
#from fast_ml.model_development import train_valid_test_split
import dgl
from curtsies import fmtfuncs as cf
import torch_geometric
import networkx as nx

from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score, accuracy_score, precision_score, recall_score
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score

import scipy.stats
import scipy.optimize

from torch.utils.data import Dataset
from torch.utils.data import DataLoader
import torch.nn.functional as F
import torch.optim as optim
import torch.nn as nn
import torch
from transformers import BertTokenizerFast as BertTokenizer, BertModel, AdamW, get_linear_schedule_with_warmup
from dgl.data import QM9EdgeDataset
import torch.distributed as dist 

from train.dist_utils import to_cuda, get_local_rank, init_distributed, seed_everything, \
    using_tensor_cores, increase_l2_fetch_granularity, WandbLogger
from torch.utils.data import DataLoader, DistributedSampler, Dataset

TYPES = {'H': 0, 'C': 1, 'N': 2, 'O': 3, 'F': 4}
ITYPES = {v:k for k, v in TYPES.items()} #inverse of atom types
REALTYPES = {'H': 1, 'C': 6, 'N': 7, 'O': 8, 'F': 9}

def get_atoms_types(graph: dgl.DGLGraph):
    return list(map(lambda inp: ITYPES.get(inp), graph.ndata["attr"][:,:5].argmax(dim=-1).tolist() )) #inputs is (batched)dgl graph-turned-list!!

def get_real_atoms_nums(atom_types: List[str]):
    assert isinstance(atom_types, list), "not a list type input..."
    return list(map(lambda inp: REALTYPES.get(inp), atom_types )) #inputs is (batched)dgl graph!!

def _get_dataloader(dataset: Dataset, shuffle: bool, collate_fn: callable, **kwargs) -> DataLoader:
    # Classic or distributed dataloader depending on the context
    sampler = DistributedSampler(dataset, shuffle=shuffle) if dist.is_initialized() else None
    return DataLoader(dataset, shuffle=(shuffle and sampler is None), sampler=sampler, collate_fn=collate_fn, **kwargs)

def _get_relative_pos(qm9_graph: dgl.DGLGraph) -> torch.Tensor:
    x = qm9_graph.ndata['pos']
    src, dst = qm9_graph.edges()
    rel_pos = x[dst] - x[src]
    return rel_pos

def _get_split_sizes(train_frac: float, full_dataset: Dataset) -> Tuple[int, int, int]:
    """DONE: Need to change split schemes!"""
    len_full = len(full_dataset)
    len_train = int(len_full * train_frac)
    len_test = int(0.1 * len_full)
    len_val = len_full - len_train - len_test
    return len_train, len_val, len_test

def geometric_to_dgl_format(z, pos, batch, cutoff=10, max_num_neighbors=32, metadata: dict=None, node_feat_dim=100, requires_grad=True):
    unique_batches = batch.unique() #LongTensor unique
    bg = []
    #pos.requires_grad_(requires_grad)
    for ub in unique_batches:
        idx = ub == batch
        batch_ = batch[idx]
        z_ = z[idx]
        pos_ = pos[idx]
        edge_index = metadata["edge_index"] if metadata != None and metadata["edge_index"] else torch_geometric.nn.radius_graph(pos_, r=cutoff, batch=batch_,
                                  max_num_neighbors=max_num_neighbors)
        row, col = edge_index
        g = dgl.graph((row, col))
        g.ndata["pos"] = pos_

        if metadata != None and metadata["atom_features"]:
            v = metadata["atom_features"][idx] #Special case for ALIGNN (nodes, node_feats)
            g.ndata["atom_features"] = v #(nodes, node_feats)
        else:
            g.ndata["atom_features"] = pos_.new_zeros(pos_.size()) #(nodes, 3)
        g.ndata["z"] = z_
        bg.append(g)

    bg = dgl.batch(bg)
    pos = bg.ndata["pos"]		
    pos.requires_grad_(requires_grad) #; leaf nodes; differentiable
    bg.ndata["pos"] = pos		
    row, col = bg.edges() 
    edge_direction = (pos[row] - pos[col]) #(edges, 3)
    bg.edata["r"] = edge_direction #(edges,3); grad_fn; differentiable
    return bg #batched dgl graphs; z, pos, r, atom_features


class DataModuleEdge(abc.ABC):
    """ Abstract DataModule. Children must define self.ds_{train | val | test}. """

    def __init__(self, **dataloader_kwargs):
        super().__init__()
        self.opt = opt = dataloader_kwargs.pop("opt")

        if get_local_rank() == 0:
            self.prepare_data()
            print(f"{get_local_rank()}-th core is parsed!")
#             self.prepare_data(opt=self.opt, data=self.data, mode=self.mode) #torch.utils.data.Dataset; useful when DOWNLOADING!

        # Wait until rank zero has prepared the data (download, preprocessing, ...)
        if dist.is_initialized():
            dist.barrier(device_ids=[get_local_rank()]) #WAITNG for 0-th core is done!
                              
        qm9_kwargs = dict(label_keys=[self.opt.task], verbose=False, raw_dir=str(self.opt.data_dir))
        full_dataset = QM9EdgeDataset(**qm9_kwargs)
        
        self.dataloader_kwargs = {'pin_memory': opt.pin_memory, 'persistent_workers': dataloader_kwargs.get('num_workers', 0) > 0,
                                 'batch_size': opt.batch_size}
        self.ds_train, self.ds_val, self.ds_test = torch.utils.data.random_split(full_dataset, _get_split_sizes(self.opt.train_frac, full_dataset),
                                                                generator=torch.Generator().manual_seed(0))

        train_targets = full_dataset.targets[self.ds_train.indices, full_dataset.label_keys[0]]
        self.targets_mean = train_targets.mean()
        self.targets_std = train_targets.std()
        
    def prepare_data(self, ):
        """ Method called only once per node. Put here any downloading or preprocessing """
#         RegDataset(opt, data, mode)
        full_dataset = QM9EdgeDataset(verbose=True, raw_dir=str(self.opt.data_dir))

    def train_dataloader(self) -> DataLoader:
        if self.opt.data_norm:
            print(cf.on_red("Be EXTREMELY careful about normalizing energy..."))
        return _get_dataloader(self.ds_train, shuffle=True, collate_fn=functools.partial(self._collate, tensor_format=self.opt.use_tensors,
                                                                                         normalize=self.opt.data_norm), **self.dataloader_kwargs)

    def val_dataloader(self) -> DataLoader:
        if self.opt.data_norm:
            print(cf.on_red("Be EXTREMELY careful about normalizing energy..."))
        return _get_dataloader(self.ds_val, shuffle=False, collate_fn=functools.partial(self._collate, tensor_format=self.opt.use_tensors,
                                                                                        normalize=self.opt.data_norm), **self.dataloader_kwargs)

    def test_dataloader(self) -> DataLoader:
        if self.opt.data_norm:
            print(cf.on_red("Be EXTREMELY careful about normalizing energy..."))
        return _get_dataloader(self.ds_test, shuffle=False, collate_fn=functools.partial(self._collate, tensor_format=self.opt.use_tensors,
                                                                                         normalize=self.opt.data_norm), **self.dataloader_kwargs)    
    @property
    def mean(self):
        return self.targets_mean

    @property
    def std(self):
        return self.targets_std
    
    def _collate(self, samples, tensor_format=True, normalize=False):
        graphs, y, *bases = map(list, zip(*samples)) #samples is a List of Lists: i.e. [(g, y), (g, y) ...]
        batched_graph = dgl.batch(graphs)
        edge_feats = {'0': batched_graph.edata['edge_attr'][..., None]}
        batched_graph.edata['rel_pos'] = _get_relative_pos(batched_graph)
        # get node features
        node_feats = {'0': batched_graph.ndata['attr'][:, :6, None]}
        targets = (torch.cat(y) - self.targets_mean) / self.targets_std if normalize else torch.cat(y) #WIP; must convert back to original!

        if bases:
            # collate bases
            all_bases = {
                key: torch.cat([b[key] for b in bases[0]], dim=0)
                for key in bases[0][0].keys()
            }

        atom_types = get_atoms_types(batched_graph)
        real_atom_nums = get_real_atoms_nums(atom_types)
        batched_graph.ndata["z"] = torch.LongTensor(real_atom_nums)
        batch_unique = torch.arange(batched_graph.batch_size) #(LongTensor of batch index)
        batch_num_nodes = batched_graph.batch_num_nodes()
        batched_graph.ndata["batch"] = torch.repeat_interleave(batch_unique, batch_num_nodes)    
        
        if bases:
            return batched_graph, node_feats, edge_feats, all_bases, targets
        elif tensor_format:
#             return batched_graph, node_feats, edge_feats, targets
            return {"z":batched_graph.ndata["z"], "pos":batched_graph.ndata["pos"], "batch":batched_graph.ndata["batch"], "E":targets, "F":targets.new_zeros(batched_graph.ndata["pos"].size())} #z, pos, batch
        else:
            return {"g": geometric_to_dgl_format(z=batched_graph.ndata["z"].long(), pos=batched_graph.ndata["pos"].float(), batch=batched_graph.ndata["batch"].long()), "E":targets.float(), "F":targets.new_zeros(batched_graph.ndata["pos"].size()).float()}

from os.path import join
from tqdm import tqdm
import torch
from torch.utils.data import Subset
# from torch_geometric.data import DataLoader
from pytorch_lightning import LightningDataModule
from pytorch_lightning.utilities import rank_zero_warn

import pathlib
root = pathlib.Path(__file__).parent.parent #argonne_gnn directory
#sys.path.append(root) #add root directory
os.chdir(root)
import data as datasets #initialized due to pathlib!

from torch_scatter import scatter
import torch.distributed as dist
import functools
from torch_geometric.data.data import BaseData

class DataModuleOthers(abc.ABC):
    def __init__(self, hparams, dataset=None):
        super(DataModuleOthers, self).__init__()
        self.hparams = hparams.__dict__ if hasattr(hparams, "__dict__") else hparams #convert argpasers to a dict
        self._mean, self._std = None, None
        self.dataset = dataset
        self.setup()
        
    def setup(self, ):
        if self.dataset is None:
            if get_local_rank() == 0:
                if self.hparams["dataset"] != "redox":
                    if self.hparams["dataset"] != "s66x8":
                        self.dataset = getattr(datasets, self.hparams["dataset"].upper())(
                                            self.hparams["data_dir"],
                                            dataset_arg=self.hparams["task"],
                                        )            #ACTUALLY getting the data and referencing it to self.dataset
                    else:
                        from data import comp6 as comp6_
                        self.dataset = getattr(comp6_, self.hparams["dataset"].upper())(
                                        self.hparams["data_dir"],
                                        dataset_arg=self.hparams["task"],
                                    )  
                else:
                    #if redox
                    from data import redox
                    self.dataset = getattr(redox, self.hparams["dataset"].upper())(
                                        self.hparams["data_dir"],
                                        dataset_arg=self.hparams["task"],
                                    )  
                    
        # Wait until rank zero has prepared the data (download, preprocessing, ...)
        if dist.is_initialized():
            dist.barrier(device_ids=[get_local_rank()]) #WAITNG for 0-th core is done!   
            
        if self.hparams["dataset"] != "redox":
            if self.hparams["dataset"] != "s66x8":
                self.dataset = getattr(datasets, self.hparams["dataset"].upper())(
                                    self.hparams["data_dir"],
                                    dataset_arg=self.hparams["task"],
                                )            #ACTUALLY getting the data and referencing it to self.dataset
            else:
                from data import comp6 as comp6_
                self.dataset = getattr(comp6_, self.hparams["dataset"].upper())(
                                self.hparams["data_dir"],
                                dataset_arg=self.hparams["task"],
                            )  
    #         self.dataloader_kwargs = {'pin_memory': self.hparams["pin_memory"], 'persistent_workers': self.hparams.get("num_workers", 0) > 0,
    #                                  'batch_size': self.hparams["batch_size"]}
        else:
            #if redox
            from data import redox
            self.dataset = getattr(redox, self.hparams["dataset"].upper())(
                                self.hparams["data_dir"],
                                dataset_arg=self.hparams["task"],
                            )             
                             
        self.dataloader_kwargs = {'pin_memory': self.hparams["pin_memory"], 'persistent_workers': False,
                                 'batch_size': self.hparams["batch_size"]}
        self.train_dataset, self.val_dataset, self.test_dataset = torch.utils.data.random_split(self.dataset,
                                                                                                _get_split_sizes(self.hparams["train_frac"], self.dataset),
                                                                generator=torch.Generator().manual_seed(0))
        
        # compute mean and standard deviation of whole training set!
        energy_batch_data = torch_geometric.data.Batch.from_data_list(self.train_dataset)
        self._mean = energy_batch_data.y.mean(dim=0) #pass to _standardize
        self._std = energy_batch_data.y.std(dim=0) #pass to _standardize
#         self._standardize() #By default (mean and std consideration); it is to get mean/std stats

    def train_dataloader(self):
#         return self._get_dataloader(self.train_dataset, "train")
        if self.hparams["data_norm"]:
            print(cf.on_red("Be EXTREMELY careful about normalizing energy..."))
        return _get_dataloader(self.train_dataset, shuffle=True, collate_fn=functools.partial(self.collate_fn, tensor_format=self.hparams["use_tensors"],
                                                                                         normalize=self.hparams["data_norm"]), **self.dataloader_kwargs)
    def val_dataloader(self):
        if self.hparams["data_norm"]:
            print(cf.on_red("Be EXTREMELY careful about normalizing energy..."))
        return _get_dataloader(self.val_dataset, shuffle=False, collate_fn=functools.partial(self.collate_fn, tensor_format=self.hparams["use_tensors"],
                                                                                         normalize=self.hparams["data_norm"]), **self.dataloader_kwargs)
    def test_dataloader(self):
        if self.hparams["data_norm"]:
            print(cf.on_red("Be EXTREMELY careful about normalizing energy..."))
        return _get_dataloader(self.test_dataset, shuffle=False, collate_fn=functools.partial(self.collate_fn, tensor_format=self.hparams["use_tensors"],
                                                                                         normalize=self.hparams["data_norm"]), **self.dataloader_kwargs)

    @property
    def atomref(self):
        if hasattr(self.dataset, "get_atomref"):
            return self.dataset.get_atomref()
        return None

    @property
    def mean(self):
        return self._mean

    @property
    def std(self):
        return self._std

#     def _get_dataloader(self, dataset, stage):
#         if stage == "train":
#             batch_size = self.hparams["batch_size"]
#             shuffle = True
#         elif stage in ["val", "test"]:
#             batch_size = self.hparams["batch_size"]
#             shuffle = False

#         sampler = DistributedSampler(dataset, shuffle=shuffle) if dist.is_initialized() else None
        
#         if self.hparams["data_norm"]:
#             print(cf.on_red("Be EXTREMELY careful about normalizing energy..."))    
            
#         dl = torch.utils.data.DataLoader(
#             dataset=dataset,
#             batch_size=batch_size,
#             shuffle=shuffle,
#             num_workers=self.hparams["num_workers"],
#             pin_memory=self.hparams["pin_memory"],
#             sampler=sampler,
#             collate_fn=functools.partial(self.collate_fn, tensor_format=self.hparams["use_tensors"], normalize=self.hparams["data_norm"])
#             ) #Not RELEVANT: torch_geometric DataLoader inherits from torch.utils.data.DataLoader! so sampler option can be passed
#         return dl

    def _standardize(self):
        def get_energy(batch, atomref):
            if batch.y is None:
                raise Exception("Missinig Energy Exception")

            if atomref is None:
                return batch["E"].clone() #this is what we get!

            # remove atomref energies from the target energy
            atomref_energy = scatter(atomref[batch.z], batch.batch, dim=0)
            return (batch.y.squeeze() - atomref_energy.squeeze()).clone()

        data = tqdm(
            _get_dataloader(self.train_dataset, "train"),
            desc="computing mean and std",
        )
        try:
            # only remove atomref energies if the atomref prior is used
            atomref = self.atomref if self.hparams["prior_model"] == "Atomref" else None #where is self.atomref defined????
            # extract energies from the data
            ys = torch.cat([get_energy(batch, atomref) for batch in data]) #Atomref is none so we get GT energy
            print(ys.mean(), ys.std())
        except Exception as e:
            rank_zero_warn(
                "Standardize is true but failed to compute dataset mean and "
                "standard deviation. Maybe the dataset only contains forces."
            )
            return

#         self._mean = ys.mean(dim=0)
#         self._std = ys.std(dim=0)
    
    def collate_fn(self, batch: List[torch_geometric.data.Data], tensor_format=True, normalize=False):
#         batch = torch_geometric.data.Batch(batch) 
#         assert isinstance(batch, torch_geometric.data.batch.DataBatch), "batch must be a BaseData instance"
        assert isinstance(batch, list) and isinstance(batch[0], BaseData), "batched input to collator is a list of torch geometric BaseData instances"
        batch = torch_geometric.data.Batch.from_data_list(batch) #batch is list of Data -> DataBatch conversion
        #ABOVE: https://pytorch-geometric.readthedocs.io/en/latest/_modules/torch_geometric/loader/dataloader.html#DataLoader:~:text=return%20Batch.from_data_list(batch%2C%20self.follow_batch%2C
        
        E = batch.y #Be careful with force! FORCE must not be normalized
        z = batch.z
        pos = batch.pos
        batches = batch.batch
        targets = (E - self.mean) / self.std if normalize else E #WIP; must convert back to original!
        target_dy = batch.dy if "dy" in batch else targets.new_zeros(pos.size()) #force; BE EXTREMELY careful when normalizinig ENERGY
        if tensor_format:
            return {"z":z, "pos":pos, "batch":batches, "E":targets, "F":target_dy} #z, pos, batch, E, F
        else:
            return {"g": geometric_to_dgl_format(z=z.long(), pos=pos.float(), batch=batches.long()), "E":targets.float(), "F":target_dy.float()}
    

"""
class PymatgenPreprocessor(PreprocessorMultiGraph):
    #https://github.com/NREL/nfp/blob/bdc69cb4790bebbb30f10f1fcf216500e93f4b8e/nfp/preprocessing/crystal_preprocessor.py
    def __init__(self, radius=None, num_neighbors=12, **kwargs):
        super(PymatgenPreprocessor, self).__init__(**kwargs)
        self.site_tokenizer = Tokenizer()
        self.radius = radius
        self.num_neighbors = num_neighbors

    def create_nx_graph(self, crystal, **kwargs) -> nx.MultiDiGraph:
        #crystal should be a pymatgen.core.Structure object
        g = nx.MultiDiGraph(crystal=crystal)
        g.add_nodes_from(((i, {"site": site}) for i, site in enumerate(crystal.sites)))

        if self.radius is None:
            # Get the expected number of sites / volume, then find a radius
            # expected to yield 2x the desired number of neighbors
            desired_vol = (crystal.volume / crystal.num_sites) * self.num_neighbors
            radius = 2 * (desired_vol / (4 * np.pi / 3)) ** (1 / 3)
        else:
            radius = self.radius

        for i, neighbors in enumerate(crystal.get_all_neighbors(radius)):
            if len(neighbors) < self.num_neighbors:
                raise RuntimeError(f"Only {len(neighbors)} neighbors for site {i}")

            sorted_neighbors = sorted(neighbors, key=lambda x: x[1])[
                : self.num_neighbors
            ]

            for _, distance, j, _ in sorted_neighbors:
                g.add_edge(i, j, distance=distance)

        return g

    def get_edge_features(
        self, edge_data: list, max_num_edges
    ) -> Dict[str, np.ndarray]:

        edge_feature_matrix = np.empty(max_num_edges, dtype="float32")
        edge_feature_matrix[:] = np.nan  # Initialize distances with nans

        for n, (_, _, edge_dict) in enumerate(edge_data):
            edge_feature_matrix[n] = edge_dict["distance"]
        return {"distance": edge_feature_matrix}

    def get_node_features(
        self, node_data: list, max_num_nodes
    ) -> Dict[str, np.ndarray]:
        site_feature_matrix = np.zeros(max_num_nodes, dtype=self.output_dtype)
        for n, site_dict in node_data:
            site_feature_matrix[n] = self.site_tokenizer(
                self.site_features(site_dict["site"])
            )
        return {"site": site_feature_matrix}

    def get_graph_features(self, graph_data: dict) -> Dict[str, np.ndarray]:
        return {}

    @property
    def site_classes(self):
        return self.site_tokenizer.num_classes + 1

    @staticmethod
    def site_features(site):
        species = site.as_dict()["species"]
        assert len(species) == 1
        return species[0]["element"]

    @property
    def output_signature(self) -> Dict[str, tf.TensorSpec]:
        if tf is None:
            raise ImportError("Tensorflow was not found")
        return {
            "site": tf.TensorSpec(shape=(None,), dtype=self.output_dtype),
            "distance": tf.TensorSpec(shape=(None,), dtype="float32"),
            "connectivity": tf.TensorSpec(shape=(None, 2), dtype=self.output_dtype),
        }

    @property
    def padding_values(self) -> Dict[str, tf.constant]:
        if tf is None:
            raise ImportError("Tensorflow was not found")
        return {
            "site": tf.constant(0, dtype=self.output_dtype),
            "distance": tf.constant(np.nan, dtype="float32"),
            "connectivity": tf.constant(0, dtype=self.output_dtype),
        }

    @property
    def tfrecord_features(self) -> Dict[str, tf.io.FixedLenFeature]:
        if tf is None:
            raise ImportError("Tensorflow was not found")
        return {
            key: tf.io.FixedLenFeature([], dtype=tf.string)
            for key in self.output_signature.keys()
        }
"""
