from kmapper import KeplerMapper, Cover
from kmapper.visuals import colorscale_from_matplotlib_cmap
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA
from sklearn.cluster import DBSCAN, KMeans
from sklearn.mixture import BayesianGaussianMixture
from umap import UMAP
from dash import Dash, dcc, html, Input, Output
import plotly.express as px
import pandas as pd 
import numpy as np
import torch
import collections
from base64 import b64encode
import plotly.graph_objects as go
from sklearn.preprocessing import MinMaxScaler 
import os
from train.dist_utils import to_cuda, get_local_rank, init_distributed, seed_everything, \
    using_tensor_cores, increase_l2_fetch_granularity, WandbLogger
from typing import *
import matplotlib.pyplot as plt

__all__ = ["preprocess", "update_graph", "roughness_index"]

def preprocess(infer_for_func: callable, df: pd.DataFrame, test_loader: torch.utils.data.DataLoader, model: torch.nn.Module, return_vecs: bool=True):
    df, X = infer_for_func(df, test_loader, model, return_vecs)

    names = df.name.values.reshape(-1, ) #(nmols)
    lat_vecs = X.astype(float) #(nmols, dim)
    preds = df.pred.values.reshape(-1, ).astype(float) #(nmols)
    reals = df.real.values.reshape(-1, ).astype(float) #(nmols)

    return names, lat_vecs, preds, reals

def get_analysis(names, lat_vecs, preds, reals,  **kwargs):
    """
    extra keywords take **seed**, **projection**, **n_clusters** and **clustering** for now...
    """
    seed = kwargs.get("random_state", 42)
    seed_everything(seed) 
    
    proj = kwargs.get("projection", "umap") #Projection Class instance
    n_components = kwargs.get("n_components", 2)
    if proj in ["umap"]:
        proj = UMAP(random_state=seed, n_components=n_components)
    elif proj in ["tsne"]:
        proj = TSNE(random_state=seed, n_components=n_components)
    elif proj in ["pca"]:
        proj = PCA(random_state=seed, n_components=n_components)
    projected = proj.fit_transform(lat_vecs) # -> (nmols, lower_dim)

    clust = kwargs.get("clustering", "kmeans") #Clustering Class instance
    n_clusters = kwargs.get("n_clusters", 8)
    if clust in ["kmeans"]:
        clust = KMeans(random_state=seed, n_clusters=n_clusters)
        clustered = clust.fit_predict(lat_vecs) # -> WIP: cluster on ORIGINAL or PROJECTED? (nmols, n_clusts)
        centroids = proj.transform(clust.cluster_centers_)
    elif clust in ["dbscan"]:
        clust = DBSCAN(n_jobs=-1)
        clustered = clust.fit_predict(lat_vecs) # -> WIP: cluster on ORIGINAL or PROJECTED? (nmols, n_clusts)
        centroids = proj.transform(clust.components_)
    elif clust in ["bgm"]:
        clust = BayesianGaussianMixture(random_state=seed, n_components=n_clusters)
        clustered = clust.fit_predict(lat_vecs) # -> WIP: cluster on ORIGINAL or PROJECTED? (nmols, n_clusts)
        centroids = proj.transform(clust.means_)

    summary = collections.namedtuple("summary", ["names", "lat_vecs", "preds", "reals", "projected", "clustered", "centroids"])
    [setattr(summary, key, val) for key, val in zip(["names", "lat_vecs", "preds", "reals", "projected", "clustered","centroids"], [names, lat_vecs, preds, reals, projected, clustered,centroids])]
    return summary #namedtuple;; make it global?

cluster_num_list = np.arange(1,20000,1)
cluster_algo_list = ["kmeans","dbscan","bgm"]
projection_algo_list = ["umap","tsne","pca"]
coloring_scheme_list = ["real","pred","clust"]
save_list = ["plotly", "image"]

# app = Dash(__name__)
# app.layout = html.Div([
#     html.Div([
#             "Number of Clusters",
#             dcc.Dropdown(
#                 options=cluster_num_list,
#                 value=1,
#                 id='choose_cluster_num'
#             ),], style={"width": "20%"}),
#     html.Br(),
#     html.Div([           
#             "Clustering Algorithm",
#             dcc.Dropdown(           
#                 options=cluster_algo_list,
#                 value="kmeans",
#                 id='choose_cluster_algo'
#             ),], style={"width": "20%"}), 
#     html.Br(),
#     html.Div([
#             "Projection Algorithm",
#             dcc.Dropdown(
#                 options=projection_algo_list,
#                 value="umap",
#                 id='choose_projection_algo'
#             ),], style={"width": "20%"}),
#     html.Br(),
#     html.Div([
#             "Coloring Scheme",
#             dcc.Dropdown(
#                 options=coloring_scheme_list,
#                 value="real",
#                 id='choose_coloring_scheme'
#             ),], style={"width": "20%"}),
#     dcc.Graph(id='graph_scatter'), 
#     ])

# @app.callback(
#     Output('graph_scatter', 'figure'),
#     Input('choose_cluster_num', 'value'),
#     Input('choose_cluster_algo', 'value'),
#     Input('choose_projection_algo', 'value'),
#     Input('choose_coloring_scheme', 'value'),
#         )


def update_graph(names, lat_vecs, preds, reals, clust_num, clust_algo, proj_algo, color_scheme, n_components, model_name):
    assert clust_num in cluster_num_list
    assert clust_algo in cluster_algo_list
    assert proj_algo in projection_algo_list
    assert color_scheme in coloring_scheme_list
    # assert render_option in save_list

    summary = get_analysis(names, lat_vecs, preds, reals, n_clusters=clust_num, clustering=clust_algo, projection=proj_algo, n_components=n_components)
    names, lat_vecs, preds, reals, projected, clustered = summary.names, summary.lat_vecs, summary.preds, summary.reals, summary.projected, summary.clustered
    # ri_mean, ri_std = roughness_index(names, lat_vecs, preds, reals, clust_num, clust_algo, proj_algo, color_scheme, n_components, model_name)

    if color_scheme in ["real"]:
        color = reals
        # color = np.stack([reals, clustered], axis=0) #2, elem 
        # print(color.shape)
    if color_scheme in ["pred"]:
        color = preds
    if color_scheme in ["clust"]:
        color = clustered #.argmax(axis=1).reshape(-1, )

    mins, maxs = np.amin(projected, axis=0), np.amax(projected, axis=0)
    margin = 5

    fig = go.Figure()

    if projected.shape[1] == 2:
        if model_name is not None:
            data = np.concatenate([names.reshape(-1,1), projected.reshape(-1,2), reals.reshape(-1,1)], axis=1)
            df = pd.DataFrame(data=data, columns=["SMILES", "x", "y", "property"])
            df.to_csv(model_name + ".csv", index=False)

        if color.ndim == 2:
            s0 = go.Scatter(
                            x=projected[:,0], y=projected[:,1],
                            hovertemplate = "<i>Name</i>: %{text} <br><b>QM Property</b>: %{customdata[0]:.3f} <br> <b>Cluster Number</b>: %{customdata[1]:.0} ",
                            text = names,
                            customdata = np.stack([reals, clustered], axis=0).tolist(),
                            showlegend = False,
                            )
            color = color[0] #QM property as color
        elif color.ndim == 1:
            s0 = go.Scatter(
                            x=projected[:,0], y=projected[:,1],
                            hovertemplate =
                            '<i>Name</i>: %{text}'+
                            '<br><b>QM Property</b>: %{customdata:.3f}<br>',
                            text = names,
                            customdata = color,
                            showlegend = False,
                            )
            color = color
        fig.add_trace(s0)

        xranges = [mins[0]-margin, maxs[0]+margin]
        yranges = [mins[1]-margin, maxs[1]+margin]

        fig.update_xaxes(
            range=xranges,  # sets the range of xaxis
            constrain="domain",  # meanwhile compresses the xaxis by decreasing its "domain"
        )
        fig.update_yaxes(
            range=yranges,
            constrain="domain",
            scaleratio = 1
        )
        fig.update_layout(
            title=f"{proj_algo.upper()}_{color_scheme.upper()}",
            xaxis_title=f"{proj_algo.upper()} 0",
            yaxis_title=f"{proj_algo.upper()} 1",
            font=dict(
                family="Courier New, monospace",
                size=18, 
                color="RebeccaPurple"
                )
            )


    elif projected.shape[1] == 3:
        if color.ndim == 2:
            s0 = go.Scatter3d(
                            x=projected[:,0], y=projected[:,1], z=projected[:,2],
                            hovertemplate = "<i>Name</i>: %{text} <br><b>QM Property</b>: %{customdata[0]:.3f} <br> <b>Cluster Number</b>: %{customdata[1]:.0} ",
                            text = names,
                            customdata = np.stack([reals, clustered], axis=0).tolist(),
                            showlegend = False,
                            )
            color = color[0] #QM property as color
        elif color.ndim == 1:
            s0 = go.Scatter3d(
                            x=projected[:,0], y=projected[:,1], z=projected[:,2],
                            hovertemplate =
                            '<i>Name</i>: %{text}'+
                            '<br><b>QM Property</b>: %{customdata:.3f}<br>',
                            text = names,
                            customdata = color,
                            showlegend = False,
                            )
            color = color
        fig.add_trace(s0)

        xranges = [mins[0]-margin, maxs[0]+margin]
        yranges = [mins[1]-margin, maxs[1]+margin]
        zranges = [mins[2]-margin, maxs[2]+margin]

        fig.update_layout(
            title=dict(text=f"{proj_algo.upper()}_{color_scheme.upper()}", font=dict(
                family="Courier New, monospace",
                size=18, 
                color="RebeccaPurple"
                )
            ),
            annotations=[],
            width=1200,
            height=1200,
            margin=dict(r=10, l=10, b=10, t=10),
            showlegend=True)
        
        #scene is part of layout now!
        fig.update_scenes(
            xaxis_title=f"{proj_algo.upper()} 0",
            yaxis_title=f"{proj_algo.upper()} 1",
            zaxis_title=f"{proj_algo.upper()} 2",
            xaxis = dict(nticks=10, range=xranges,
                        backgroundcolor="rgba(80, 70, 70, 0.5)",
                        gridcolor="white",
                        showbackground=True,
                        zerolinecolor="white",
                        ),
            yaxis = dict(nticks=10, range=yranges,
                        backgroundcolor="rgba(70, 80, 70, 0.5)",
                        gridcolor="white",
                        showbackground=True,
                        zerolinecolor="white",
                        ),
            zaxis = dict(nticks=10, range=zranges,
                        backgroundcolor="rgba(70, 70, 80, 0.5)",
                        gridcolor="white",
                        showbackground=True,
                        zerolinecolor="white",)        
        )
        
    fig.update_traces(mode="markers",                     
                        marker=dict(
                        color=MinMaxScaler(feature_range=(0,255)).fit_transform(color.reshape(-1,1)).reshape(-1, ),
                        colorscale='jet',
                        showscale=False),
                        )
    # fig = px.scatter(x=projected[:,0], y=projected[:,1], color=color, range_x=xranges, range_y=yranges)

    fig.write_image(os.path.join(os.getcwd(), "publication_figures", "plotly_fig.png"))
    return fig, summary

        # img_bytes = fig.to_image(format="png")
        # encoding = b64encode(img_bytes).decode()
        # img_b64 = "data:image/png;base64," + encoding
        # return html.Img(src=img_b64, style={'height': '500px'})

def roughness_index(names, lat_vecs, preds, reals, clust_num, clust_algo, proj_algo, color_scheme, n_components, model_name):
    assert clust_num in cluster_num_list
    assert clust_algo in cluster_algo_list
    assert proj_algo in projection_algo_list
    assert color_scheme in coloring_scheme_list
    # assert render_option in save_list

    summary = get_analysis(names, lat_vecs, preds, reals, n_clusters=clust_num, clustering=clust_algo, projection=proj_algo, n_components=n_components)
    names, lat_vecs, preds, reals, projected, clustered = summary.names, summary.lat_vecs, summary.preds, summary.reals, summary.projected, summary.clustered

    from rogi import RoughnessIndex
    #https://github.com/coleygroup/rogi/blob/main/src/rogi/roughness_index.py
    means, stds, metrics = [], [], ['euclidean', 'cityblock', 'cosine'] #mahalanobis has non-invertible matrix!
    for metric in metrics:
        ri = RoughnessIndex(Y=reals.astype(float), X=lat_vecs.astype(float), norm_Y=True, metric=metric, max_dist=None)
        mean, std = ri.compute_index(nboots=10)
        means.append(mean)
        stds.append(std)
    return np.array(means), np.array(stds), metrics

# app.run_server(debug=True, host = '127.0.0.1')
