import contextlib
import io
import pickle
import sys
from typing import Optional, Union
import numpy as np
from ase import Atoms, units
from ase.calculators.calculator import Calculator, all_changes
from ase.constraints import ExpCellFilter
from ase.io import Trajectory
from ase.md.nptberendsen import NPTBerendsen
from ase.md.nvtberendsen import NVTBerendsen
from ase.optimize.bfgs import BFGS
from ase.optimize.bfgslinesearch import BFGSLineSearch
from ase.optimize.fire import FIRE
from ase.optimize.lbfgs import LBFGS, LBFGSLineSearch
from ase.optimize.mdmin import MDMin
from ase.optimize.optimize import Optimizer
from ase.optimize.sciopt import SciPyFminBFGS, SciPyFminCG
from pymatgen.core.structure import Molecule, Structure
from pymatgen.io.ase import AseAtomsAdaptor
import torch
from ase import units
from ase.calculators.calculator import Calculator, all_changes
from ase.io import read, write
from ase.io.trajectory import Trajectory
from ase.io.xyz import read_xyz, write_xyz
from ase.md import VelocityVerlet, Langevin, MDLogger
from ase.md.velocitydistribution import (
    MaxwellBoltzmannDistribution,
    Stationary,
    ZeroRotation,
)
from ase.optimize import QuasiNewton
from ase.vibrations import Vibrations
import os
import MDAnalysis as mda


#https://github.com/materialsvirtuallab/m3gnet/blob/main/m3gnet/models/_dynamics.py
# https://github.com/atomistic-machine-learning/schnetpack/blob/f2cf162b2b2810a850a5856e46a18b61822515ee/src/schnetpack/interfaces/ase_interface.py#L208

__all__ = ["AseInterface", "CustomCalculator"]

class CustomCalculator(Calculator):
    implemented_properties = ["energy", "forces"]

    def __init__(
        self,
        model,
        device="cpu",
        energy=True,
        forces=True,
        atoms=None,
        restart=None,
        add_atom_energies=False,
        label="custom_calc",  # ase settings
        **kwargs
    ):
        super().__init__(**kwargs)

        self.model = model
        self.model.to(device)
        self.model_energy = energy
        self.model_forces = forces

    def calculate(self, atoms=None, properties=["energy", "force"], system_changes=all_changes):
        """
        Args:
            atoms (ase.Atoms): ASE atoms object.
            properties (list of str): do not use this, no functionality
            system_changes (list of str): List of changes for ASE.
        """
        # First call original calculator to set atoms attribute
        # (see https://wiki.fysik.dtu.dk/ase/_modules/ase/calculators/calculator.html#Calculator)

        super().calculate(atoms, properties, system_changes)
        model_inputs = dict(z=torch.from_numpy(atoms.numbers).view(-1,).long(), pos=torch.from_numpy(atoms.positions).view(-1,3).float(), batch=torch.LongTensor([0]*len(atoms.numbers)).view(-1).long() )
        # Call model
        model_results = self.model(**model_inputs) #e and f
        energy, forces = model_results
        results = {}
        
        # Convert outputs to calculator format
        results["energy"] = energy.detach().cpu().item() #kcal/mol for MD17
          # ase calculator should return scalar energy

        results["forces"] = forces.view(len(atoms), 3).detach().cpu().numpy() #kcal/mol/A for MD17
        
        self.results = results

class AseInterface:
    """
    Interface for ASE calculations (optimization and molecular dynamics)
    Args:
        molecule_path (str): Path to initial geometry
        ml_model (object): Trained model
        working_dir (str): Path to directory where files should be stored
        device (str): cpu or cuda
    """

    def __init__(
        self,
        molecule_path: str,
        ml_model: torch.nn.Module,
        working_dir: str,
        device="cpu",
        energy=True,
        forces=True
    ):
        # Setup directory
        self.working_dir = working_dir
        if not os.path.exists(self.working_dir):
            os.makedirs(self.working_dir)
        self.molecule_path = molecule_path

        # Load the molecule
        self.molecule = None
        self._load_molecule(os.path.join(self.working_dir, molecule_path))

        # Set up calculator

        self.molecule: ase.Atoms = Atoms(positions=self.molecule.get_positions(), numbers=self.molecule.numbers, pbc=True)
        calculator = CustomCalculator(
            ml_model,
            device=device,
            energy=energy,
            forces=forces,
            atoms=self.molecule)
        self.molecule.calc = calculator

        # Unless initialized, set dynamics to False
        self.dynamics = False

    def _load_molecule(self, molecule_path):
        """
        Load molecule from file (can handle all ase formats).
        Args:
            molecule_path (str): Path to molecular geometry
        """
        file_format = os.path.splitext(molecule_path)[-1]
#         if file_format == "xyz":
#             molecule_path = open(molecule_path, "r") #file_obj
#             self.molecule = read_xyz(molecule_path, 0)
#         else:
        self.molecule = read(molecule_path)

    def save_molecule(self, name, file_format="xyz", append=False):
        """
        Save the current molecular geometry.
        Args:
            name (str): Name of save-file.
            file_format (str): Format to store geometry (default xyz).
            append (bool): If set to true, geometry is added to end of file
                (default False).
        """
        molecule_path = os.path.join(self.working_dir, "%s.%s" % (name, file_format))
        write(molecule_path, self.molecule, format=file_format, append=append)

    def calculate_single_point(self):
        """
        Perform a single point computation of the energies and forces and
        store them to the working directory. The format used is the extended
        xyz format. This functionality is mainly intended to be used for
        interfaces.
        """
        energy = self.molecule.get_potential_energy()
        forces = self.molecule.get_forces()
        self.molecule.energy = energy
        self.molecule.forces = forces

        self.save_molecule("single_point", file_format="extxyz")

    def init_md(
        self,
        name,
        time_step=0.1,
        temp_init=300,
        temp_bath=None,
        reset=False,
        interval=1,
    ):
        """
        Initialize an ase molecular dynamics trajectory. The logfile needs to
        be specifies, so that old trajectories are not overwritten. This
        functionality can be used to subsequently carry out equilibration and
        production.
        Args:
            name (str): Basic name of logfile and trajectory
            time_step (float): Time step in fs (default=0.5)
            temp_init (float): Initial temperature of the system in K
                (default is 300)
            temp_bath (float): Carry out Langevin NVT dynamics at the specified
                temperature. If set to None, NVE dynamics are performed
                instead (default=None)
            reset (bool): Whether dynamics should be restarted with new initial
                conditions (default=False)
            interval (int): Data is stored every interval steps (default=1)
        """

        # If a previous dynamics run has been performed, don't reinitialize
        # velocities unless explicitly requested via reset=True
        if not self.dynamics or reset:
            self._init_velocities(temp_init=temp_init)

        # Set up dynamics
        if temp_bath is None:
            self.dynamics = VelocityVerlet(self.molecule, time_step * units.fs)
        else:
            self.dynamics = Langevin(
                atoms=self.molecule,
                timestep=time_step * units.fs,
                temperature_K=temp_bath,
                friction=1.0 / (100.0 * units.fs),
            )

        # Create monitors for logfile and a trajectory file
        logfile = os.path.join(self.working_dir, "%s.log" % name)
        trajfile = os.path.join(self.working_dir, "%s.traj" % name)
        logger = MDLogger(
            self.dynamics,
            self.molecule,
            logfile,
            stress=False,
            peratom=False,
            header=True,
            mode="a",
        )
        trajectory = Trajectory(trajfile, "w", self.molecule)

        # Attach monitors to trajectory
        self.dynamics.attach(logger, interval=interval)
        self.dynamics.attach(trajectory.write, interval=interval)

    def _init_velocities(
        self, temp_init=300, remove_translation=True, remove_rotation=True
    ):
        """
        Initialize velocities for molecular dynamics
        Args:
            temp_init (float): Initial temperature in Kelvin (default 300)
            remove_translation (bool): Remove translation components of
                velocity (default True)
            remove_rotation (bool): Remove rotation components of velocity
                (default True)
        """
#         MaxwellBoltzmannDistribution(self.molecule, temp_init * units.kB)
        MaxwellBoltzmannDistribution(self.molecule, temperature_K=temp_init)

        if remove_translation:
            Stationary(self.molecule)
        if remove_rotation:
            ZeroRotation(self.molecule)

    def run_md(self, steps):
        """
        Perform a molecular dynamics simulation using the settings specified
        upon initializing the class.
        Args:
            steps (int): Number of simulation steps performed
        """
        if not self.dynamics:
            raise AttributeError(
                "Dynamics need to be initialized using the" " 'setup_md' function"
            )

        self.dynamics.run(steps)
 
    def optimize(self, fmax=1.0e-2, steps=1000):
        """
        Optimize a molecular geometry using the Quasi Newton optimizer in ase
        (BFGS + line search)
        Args:
            fmax (float): Maximum residual force change (default 1.e-2)
            steps (int): Maximum number of steps (default 1000)
        """
        name = "optimization"
        optimize_file = os.path.join(self.working_dir, name)
        optimizer = QuasiNewton(
            self.molecule,
            trajectory="%s.traj" % optimize_file,
            restart="%s.pkl" % optimize_file,
        )
        optimizer.run(fmax, steps)

        # Save final geometry in xyz format
        self.save_molecule(name)

    def compute_normal_modes(self, write_jmol=True):
        """
        Use ase calculator to compute numerical frequencies for the molecule
        Args:
            write_jmol (bool): Write frequencies to input file for
                visualization in jmol (default=True)
        """
        freq_file = os.path.join(self.working_dir, "normal_modes")

        # Compute frequencies
        frequencies = Vibrations(self.molecule, name=freq_file)
        frequencies.run()

        # Print a summary
        frequencies.summary()

        # Write jmol file if requested
        if write_jmol:
            frequencies.write_jmol()
            
    def convert_to_mdanalysis(self, ase_traj: str=None, slices=slice(0,None,1), save_format="dcd"):
        assert os.path.exists(ase_traj), "ASE traj does not exist... Run MD or Optimization first!"
        traj = Trajectory(ase_traj)[slices] #Trajectory ==> ase.io.trajectory.SlicedTrajectory
        coords = np.stack([atom.positions for atom in traj], axis=0) #Frames, L, 3
        ase_molecule = os.path.join(self.working_dir, self.molecule_path) #xyz file
        
        u = mda.Universe(ase_molecule) #universe
        u.load_new(coords) #overwrite coords
        ase_traj_name = os.path.split(ase_traj)[-1].split(".")[0] #filename w/o/ extenstion...
        mda_traj_name = ase_traj_name + "." + save_format #[filename].dcd
        
        with mda.Writer(mda_traj_name, u.atoms.n_atoms) as w:
            for ts in u.trajectory:
                w.write(u.atoms)        
        
        return u #mda.Universe

    @staticmethod
    def convert_to_mdanalysis_static(ase_traj: str=None, slices=slice(0,None,1), save_format="dcd", working_dir=None, molecule_path=None):
        assert os.path.exists(ase_traj), "ASE traj does not exist... Run MD or Optimization first!"
        traj = Trajectory(ase_traj)[slices] #Trajectory ==> ase.io.trajectory.SlicedTrajectory
        coords = np.stack([atom.positions for atom in traj], axis=0) #Frames, L, 3
        ase_molecule = os.path.join(working_dir, molecule_path) #xyz file
        
        u = mda.Universe(ase_molecule) #universe
        u.load_new(coords) #overwrite coords
        ase_traj_name = os.path.split(ase_traj)[-1].split(".")[0] #filename w/o/ extenstion...
        mda_traj_name = ase_traj_name + "." + save_format #[filename].dcd
        
        with mda.Writer(mda_traj_name, u.atoms.n_atoms) as w:
            for ts in u.trajectory:
                w.write(u.atoms)        
        
        return u #mda.Universe


# OPTIMIZERS = {
#     "FIRE": FIRE,
#     "BFGS": BFGS,
#     "LBFGS": LBFGS,
#     "LBFGSLineSearch": LBFGSLineSearch,
#     "MDMin": MDMin,
#     "SciPyFminCG": SciPyFminCG,
#     "SciPyFminBFGS": SciPyFminBFGS,
#     "BFGSLineSearch": BFGSLineSearch,
# }

# class M3GNetCalculator(Calculator):
#     """
#     M3GNet calculator based on ase Calculator
#     """

#     implemented_properties = ["energy", "free_energy", "forces", "stress"]

#     def __init__(self, potential: Potential, compute_stress: bool = False, stress_weight: float = 1.0, **kwargs):
#         """
#         Args:
#             potential (Potential): m3gnet.models.Potential
#             compute_stress (bool): whether to calculate the stress
#             stress_weight (float): the stress weight.
#             **kwargs:
#         """
#         super().__init__(**kwargs)
#         self.potential = potential
#         self.compute_stress = compute_stress
#         self.stress_weight = stress_weight

#     def calculate(
#         self,
#         atoms: Optional[Atoms] = None,
#         properties: Optional[list] = ["energy", "force"],
#         system_changes: Optional[list] = None,
#     ):
#         """
#         Args:
#             atoms (ase.Atoms): ase Atoms object
#             properties (list): list of properties to calculate
#             system_changes (list): monitor which properties of atoms were
#                 changed for new calculation. If not, the previous calculation
#                 results will be loaded.
#         Returns:
#         """
#         properties = properties or ["energy"]
#         system_changes = system_changes or all_changes
#         super().calculate(atoms=atoms, properties=properties, system_changes=system_changes)

#         graph = self.potential.graph_converter(atoms)
#         graph_list = graph.as_tf().as_list()
#         results = self.potential.get_efs_tensor(graph_list, include_stresses=self.compute_stress)
#         self.results.update(
#             energy=results[0].numpy().ravel(),
#             free_energy=results[0].numpy().ravel(),
#             forces=results[1].numpy(),
#         )
#         if self.compute_stress:
#             self.results.update(stress=results[2].numpy()[0] * self.stress_weight)

# class Relaxer:
#     """
#     Relaxer is a class for structural relaxation
#     """

#     def __init__(
#         self,
#         potential: Optional[Union[Potential, str]] = None,
#         optimizer: Union[Optimizer, str] = "FIRE",
#         relax_cell: bool = True,
#         stress_weight: float = 0.01,
#     ):
#         """
#         Args:
#             potential (Optional[Union[Potential, str]]): a potential,
#                 a str path to a saved model or a short name for saved model
#                 that comes with M3GNet distribution
#             optimizer (str or ase Optimizer): the optimization algorithm.
#                 Defaults to "FIRE"
#             relax_cell (bool): whether to relax the lattice cell
#             stress_weight (float): the stress weight for relaxation
#         """
#         if isinstance(potential, str):
#             potential = Potential(M3GNet.load(potential))
#         if potential is None:
#             potential = Potential(M3GNet.load())

#         if isinstance(optimizer, str):
#             optimizer_obj = OPTIMIZERS.get(optimizer, None)
#         elif optimizer is None:
#             raise ValueError("Optimizer cannot be None")
#         else:
#             optimizer_obj = optimizer

#         self.opt_class: Optimizer = optimizer_obj
#         self.calculator = M3GNetCalculator(potential=potential, stress_weight=stress_weight)
#         self.relax_cell = relax_cell
#         self.potential = potential
#         self.ase_adaptor = AseAtomsAdaptor()

#     def relax(
#         self,
#         atoms: Atoms,
#         fmax: float = 0.1,
#         steps: int = 500,
#         traj_file: str = None,
#         interval=1,
#         verbose=False,
#         **kwargs,
#     ):
#         """
#         Args:
#             atoms (Atoms): the atoms for relaxation
#             fmax (float): total force tolerance for relaxation convergence.
#                 Here fmax is a sum of force and stress forces
#             steps (int): max number of steps for relaxation
#             traj_file (str): the trajectory file for saving
#             interval (int): the step interval for saving the trajectories
#             **kwargs:
#         Returns:
#         """
#         if isinstance(atoms, (Structure, Molecule)):
#             atoms = self.ase_adaptor.get_atoms(atoms)
#         atoms.set_calculator(self.calculator)
#         stream = sys.stdout if verbose else io.StringIO()
#         with contextlib.redirect_stdout(stream):
#             obs = TrajectoryObserver(atoms)
#             if self.relax_cell:
#                 atoms = ExpCellFilter(atoms)
#             optimizer = self.opt_class(atoms, **kwargs)
#             optimizer.attach(obs, interval=interval)
#             optimizer.run(fmax=fmax, steps=steps)
#             obs()
#         if traj_file is not None:
#             obs.save(traj_file)
#         if isinstance(atoms, ExpCellFilter):
#             atoms = atoms.atoms

#         return {
#             "final_structure": self.ase_adaptor.get_structure(atoms),
#             "trajectory": obs,
#         }


# class TrajectoryObserver:
#     """
#     Trajectory observer is a hook in the relaxation process that saves the
#     intermediate structures
#     """

#     def __init__(self, atoms: Atoms):
#         """
#         Args:
#             atoms (Atoms): the structure to observe
#         """
#         self.atoms = atoms
#         self.energies: list[float] = []
#         self.forces: list[np.ndarray] = []
#         self.stresses: list[np.ndarray] = []
#         self.atom_positions: list[np.ndarray] = []
#         self.cells: list[np.ndarray] = []

#     def __call__(self):
#         """
#         The logic for saving the properties of an Atoms during the relaxation
#         Returns:
#         """
#         self.energies.append(self.compute_energy())
#         self.forces.append(self.atoms.get_forces())
#         self.stresses.append(self.atoms.get_stress())
#         self.atom_positions.append(self.atoms.get_positions())
#         self.cells.append(self.atoms.get_cell()[:])

#     def compute_energy(self) -> float:
#         """
#         calculate the energy, here we just use the potential energy
#         Returns:
#         """
#         energy = self.atoms.get_potential_energy()
#         return energy

#     def save(self, filename: str):
#         """
#         Save the trajectory to file
#         Args:
#             filename (str): filename to save the trajectory
#         Returns:
#         """
#         with open(filename, "wb") as f:
#             pickle.dump(
#                 {
#                     "energy": self.energies,
#                     "forces": self.forces,
#                     "stresses": self.stresses,
#                     "atom_positions": self.atom_positions,
#                     "cell": self.cells,
#                     "atomic_number": self.atoms.get_atomic_numbers(),
#                 },
#                 f,
#             )


# class MolecularDynamics:
#     """
#     Molecular dynamics class
#     """

#     def __init__(
#         self,
#         atoms: Atoms,
#         potential: Union[Potential, str] = None
#         ensemble: str = "nvt",
#         temperature: int = 300,
#         timestep: float = 1.0,
#         pressure: float = 1.01325 * units.bar,
#         taut: Optional[float] = None,
#         taup: Optional[float] = None,
#         compressibility_au: Optional[float] = None,
#         trajectory: Optional[Union[str, Trajectory]] = None,
#         logfile: Optional[str] = None,
#         loginterval: int = 1,
#         append_trajectory: bool = False,
#     ):
#         """
#         Args:
#             atoms (Atoms): atoms to run the MD
#             potential (Potential): potential for calculating the energy, force,
#                 stress of the atoms
#             ensemble (str): choose from 'nvt' or 'npt'. NPT is not tested,
#                 use with extra caution
#             temperature (float): temperature for MD simulation, in K
#             timestep (float): time step in fs
#             pressure (float): pressure in eV/A^3
#             taut (float): time constant for Berendsen temperature coupling
#             taup (float): time constant for pressure coupling
#             compressibility_au (float): compressibility of the material in A^3/eV
#             trajectory (str or Trajectory): Attach trajectory object
#             logfile (str): open this file for recording MD outputs
#             loginterval (int): write to log file every interval steps
#             append_trajectory (bool): Whether to append to prev trajectory
#         """

#         assert isinstance(potential, torch.nn.Module):
#             potential = potential

#         if isinstance(atoms, (Structure, Molecule)):
#             atoms = AseAtomsAdaptor().get_atoms(atoms)
#         self.atoms = atoms #ase atoms
#         self.atoms.set_calculator(M3GNetCalculator(potential=potential))

#         if taut is None:
#             taut = 100 * timestep * units.fs
#         if taup is None:
#             taup = 1000 * timestep * units.fs

#         if ensemble.lower() == "nvt":
#             self.dyn = NVTBerendsen(
#                 self.atoms,
#                 timestep * units.fs,
#                 temperature_K=temperature,
#                 taut=taut,
#                 trajectory=trajectory,
#                 logfile=logfile,
#                 loginterval=loginterval,
#                 append_trajectory=append_trajectory,
#             )

#         elif ensemble.lower() == "npt":
#             self.dyn = NPTBerendsen(
#                 self.atoms,
#                 timestep * units.fs,
#                 temperature_K=temperature,
#                 pressure_au=pressure,
#                 taut=taut,
#                 taup=taup,
#                 compressibility_au=compressibility_au,
#                 trajectory=trajectory,
#                 logfile=logfile,
#                 loginterval=loginterval,
#                 append_trajectory=append_trajectory,
#             )
#         else:
#             raise ValueError("Ensemble not supported")

#         self.trajectory = trajectory
#         self.logfile = logfile
#         self.loginterval = loginterval
#         self.timestep = timestep

#     def run(self, steps: int):
#         """
#         Thin wrapper of ase MD run
#         Args:
#             steps (int): number of MD steps
#         Returns:
#         """
#         self.dyn.run(steps)

#     def set_atoms(self, atoms: Atoms):
#         """
#         Set new atoms to run MD
#         Args:
#             atoms (Atoms): new atoms for running MD
#         Returns:
#         """
#         calculator = self.atoms.calc
#         self.atoms = atoms
#         self.dyn.atoms = atoms
#         self.dyn.atoms.set_calculator(calculator)

        

