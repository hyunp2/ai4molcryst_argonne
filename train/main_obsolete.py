import os, abc, sys
import pdb
import glob
import copy
import wandb
import h5py
import random
import string
import argparse
import pandas as pd
import numpy as np
from scipy.special import softmax
from PIL import Image
from typing import *
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from fast_ml.model_development import train_valid_test_split

from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score, accuracy_score, precision_score, recall_score
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score

import scipy.stats
#import scipy.signal
#import scipy.optimize
import importlib

from torch.utils.data import Dataset
from torch.utils.data import DataLoader
import torch.nn.functional as F
import torch.optim as optim
import torch.nn as nn
import torch
from transformers import BertTokenizerFast as BertTokenizer, BertModel, AdamW, get_linear_schedule_with_warmup
import pathlib

from schnet import schnet
from physnet import physnet
from alignn import alignn
from torchmdnet import torchmdnet
from dimenet import dimenet

import captum
#from apex.optimizers import FusedAdam, FusedLAMB
try:
    importlib.import_module("apex.optimizers")
    from apex.optimizers import FusedAdam, FusedLAMB
except Exception as e:
    pass
from train.general_train_eval import train as train_nvidia
from train.dist_utils import to_cuda, get_local_rank, init_distributed, seed_everything, \
    using_tensor_cores, increase_l2_fetch_granularity, WandbLogger
from torch.utils.data import DataLoader, DistributedSampler, Dataset
from train.general_train_eval import load_state, save_state, single_test
from train.dataloader import DataModule
from train.loss_utils import get_loss_func
from train.gpu_affinity import *

def get_parser():
    parser = argparse.ArgumentParser()
#     parser.add_argument('--name', type=str, default=''.join(random.choice(string.ascii_lowercase) for i in range(10)))
    parser.add_argument('--name', type=str, default=None)
    parser.add_argument('--seed', type=int, default=7)
    parser.add_argument('--gpu', action='store_true')
    parser.add_argument('--gpus', action='store_true')
    parser.add_argument('--silent', action='store_true')
    parser.add_argument('--log', action='store_true') #only returns true when passed in bash
    parser.add_argument('--plot', action='store_true')

    # data
    parser.add_argument('--train_test_ratio', type=float, default=0.02)
    parser.add_argument('--train_val_ratio', type=float, default=0.03)
    parser.add_argument('--train_frac', type=float, default=1.0)
    parser.add_argument('--warm_up_split', type=int, default=5)
    parser.add_argument('--batches', type=int, default=160)
    parser.add_argument('--test_samples', type=int, default=5) # -1 for all
    parser.add_argument('--test_steps', type=int, default=100)
    parser.add_argument('--data_norm', action='store_true')
    parser.add_argument('--data_dir', type=str, default="/lus/grand/projects/AIHacks/gpuHack/data/")
    parser.add_argument('--task', type=str, default="homo")

    # train
    parser.add_argument('--epoches', type=int, default=2)
    parser.add_argument('--batch_size', type=int, default=128) #Per GPU batch size
    parser.add_argument('--num_workers', type=int, default=4)
    parser.add_argument('--learning_rate','-lr', type=float, default=1e-4)
    parser.add_argument('--weight_decay', type=float, default=2e-5)
    parser.add_argument('--dropout', type=float, default=0)
    parser.add_argument('--resume', action='store_true')
    parser.add_argument('--distributed',  action="store_true")
    parser.add_argument('--low_memory',  action="store_true")
    parser.add_argument('--amp', action="store_true", help="floating 16 when turned on.")
    parser.add_argument('--loss_schedule', '-ls', type=str, choices=["manual", "lrannealing", "softadapt", "relobralo", "gradnorm"], help="how to adjust loss weights.")
    parser.add_argument('--with_force', type=bool, default=False)
    parser.add_argument('--optimizer', type=str, default='adam', choices=["adam","lamb","sgd"])
    parser.add_argument('--gradient_clip', type=float, default=None) 
    parser.add_argument('--accumulate_grad_batches', type=int, default=1) 
    parser.add_argument(
        "--not_use_env",
        default=False,
        action="store_false",
        help="Use environment variable to pass "
        "'local rank'. For legacy reasons, the default value is False. "
        "If set to True, the script will not pass "
        "--local_rank as argument, and will instead set LOCAL_RANK.",
    )

    # model
    parser.add_argument('--backbone', type=str, default='physnet', choices=["schnet","physnet","torchmdnet","alignn","dimenet","dimenetpp"])
    parser.add_argument('--load_ckpt_path', type=str, default="/lus/grand/projects/AIHacks/gpuHack/save/")
    parser.add_argument('--explain', type=bool, default=False, help="gradient hook for CAM...") #Only for Schnet.Physnet.Alignn WIP!

    opt = parser.parse_args()

    return opt

BACKBONES = {
			"schnet": schnet.SchNet,
			 "physnet": physnet.Physnet,
			 "torchmdnet": torchmdnet.TorchMD_Net,
			 "alignn": alignn.iCGCNN,
			 "dimenet": dimenet.DimeNet,
			"dimenetpp": dimenet.DimeNetPlusPlus,
			}

#WIP for TorchMDNet Configs!
BACKBONE_KWARGS = {
			"schnet": dict(hidden_channels=128, 
				 num_filters=128,
				 num_interactions=6,
				 num_gaussians=50,
				 cutoff=10.0,
				 max_num_neighbors=32,
				 readout='add',
				 dipole=False,
				 mean=None,
				 std=None,
				 atomref=None),
			 "physnet": dict(dfilter=64, 
					 filter=64, 
					 cutoff=10, 
					 num_residuals=3,
					 num_residuals_atomic=2, 
					 num_interactions=5, 
					 activation_fn=torch.nn.ReLU(), 
					 dmodel=64, 
					 token_embedding_necessary=True, 
					 max_num_neighbors=32),
			 "torchmdnet": torchmdnet.TorchMD_Net, 
			 "alignn": alignn.ICGCNNConfig(name="icgcnn"),
			 "dimenet": dict(hidden_channels=128,
					    out_channels=1,
					    num_blocks=4,
					    int_emb_size=64,
					    basis_emb_size=8,
					    out_emb_channels=256,
					    num_spherical=7,
					    num_radial=6,
					    cutoff=5.0,
					    max_num_neighbors=32,
					    envelope_exponent=5,
					    num_before_skip=1,
					    num_after_skip=2,
					    num_output_layers=3,),
			 "dimenetpp": dict(hidden_channels=128,
					    out_channels=1,
					    num_blocks=4,
					    int_emb_size=64,
					    basis_emb_size=8,
					    out_emb_channels=256,
					    num_spherical=7,
					    num_radial=6,
					    cutoff=5.0,
					    max_num_neighbors=32,
					    envelope_exponent=5,
					    num_before_skip=1,
					    num_after_skip=2,
					    num_output_layers=3,)}

def run():
    """
    train() -> train_nvidia -> train_epoch
    
    This function must define a Normal Model, DistSampler etc.
    Then, INSIDE train_nvidia, DDP Model, DDP optimizer, set_epoch for DistSampler, GradScaler etc. (and all_gather etc) are fired up.
    Then inside train_epoch, Loss/Backprop is done
    """
    is_distributed = init_distributed()
    local_rank = get_local_rank()
    opt = get_parser()
    
    if opt.log:
        logger = WandbLogger(name=None, entity="hyunp2", project='ArgonneGNN')

    print('Backbone {} With_force {}'.format(opt.backbone, opt.with_force))

    #Distributed Sampler Loader
    datamodule= DataModule(opt=opt)
    train_loader = datamodule.train_dataloader()
    val_loader = datamodule.val_dataloader()
    test_loader = datamodule.test_dataloader()

    #Model 
    model = BACKBONES.get(opt.backbone, physnet.Physnet) #Uninitialized class
    model_kwargs = BACKBONE_KWARGS.get(opt.backbone, None) #TorchMDNet not yet!

    if opt.backbone != "alignn": model_kwargs.update({"explain": opt.explain})  
    else: model_kwargs.explain = opt.explain #only for alignn net (due to pydantic)
	
    model = model(**model_kwargs) if opt.backbone != "alignn" else model(model_kwargs) #Accounting for alignn net
	
    if opt.gpu:
        model = model.to(torch.cuda.current_device())
    
    #Dist training
    if is_distributed:         
        nproc_per_node = torch.cuda.device_count()
        affinity = set_affinity(local_rank, nproc_per_node)
    increase_l2_fetch_granularity()

    train_nvidia(model=model,
          train_dataloader=train_loader,
          val_dataloader=val_loader,
          test_dataloader=test_loader,
          logger=logger,
          get_loss_func=get_loss_func,
          args=opt)
    

if __name__ == "__main__":
    run()
#     print("main.py")

#     python -m train.main --log --backbone physnet --with_force False --gpu --name gnn			 #add --resume if needed
#     python -m torch.distributed.run --nnodes=1 --nproc_per_node=gpu --max_restarts 0 --module train.main --log --backbone physnet --with_force False --gpu --name gnn
    
