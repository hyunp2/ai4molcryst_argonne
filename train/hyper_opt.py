import os, abc, sys
import pdb
import glob
import copy
import wandb
import h5py
import random
import string
import argparse
import pandas as pd
import numpy as np
from scipy.special import softmax
from PIL import Image
from typing import *
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from fast_ml.model_development import train_valid_test_split

from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score, accuracy_score, precision_score, recall_score
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score

from curtsies import fmtfuncs as cf
import scipy.stats
import torchmetrics

import torchvision.transforms as transforms
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
import torch.nn.functional as F
import torch.optim as optim
import torch.nn as nn
import torch
import torchvision
from transformers import BertTokenizerFast as BertTokenizer, BertModel, AdamW, get_linear_schedule_with_warmup

import pathlib
root = pathlib.Path(__file__).parent.parent #gpuHack directory
#sys.path.append(root) #add root directory
os.chdir(root)
from schnet import schnet
from physnet import physnet
from alignn import alignn
from torchmdnet import torchmdnet
from dimenet import dimenet
from torchmdnet import torchmdnet

import captum
from apex.optimizers import FusedAdam, FusedLAMB
from train.general_train_eval import train as train_nvidia
from train.dist_utils import to_cuda, get_local_rank, init_distributed, seed_everything, \
    using_tensor_cores, increase_l2_fetch_granularity, WandbLogger
from torch.utils.data import DataLoader, DistributedSampler, Dataset
from train.general_train_eval import load_state, save_state, single_train, single_val, single_test
from train.dataloader import DataModuleEdge
from main import get_parser
from train.loss_utils import get_loss_func

""""0. DEFINE MODEL CONFIGS"""
BACKBONES = {
			"schnet": schnet.SchNet,
			 "physnet": physnet.Physnet,
			 "torchmdnet": torchmdnet.TorchMD_Net,
			 "alignn": alignn.ALIGNN,
			 "dimenet": dimenet.DimeNet,
			"dimenetpp": dimenet.DimeNetPlusPlus,
			}

BACKBONE_KWARGS = {
			"schnet": dict(hidden_channels=128, 
				 num_filters=128,
				 num_interactions=6,
				 num_gaussians=50,
				 cutoff=10.0,
				 max_num_neighbors=32,
				 readout='add',
				 dipole=False,
				 mean=None,
				 std=None,
				 atomref=None),
			 "physnet": dict(dfilter=64, 
					 filter=64, 
					 cutoff=10, 
					 num_residuals=3,
					 num_residuals_atomic=2, 
					 num_interactions=5, 
					 activation_fn=torch.nn.ReLU(), 
					 dmodel=64, 
					 token_embedding_necessary=True, 
					 max_num_neighbors=32),
			 "torchmdnet": dict(activation= "silu",
					aggr= "add",
					atom_filter= -1,
					attn_activation= "silu",
					batch_size= 128,
					coord_files= "null",
					cutoff_lower= 0.0,
					cutoff_upper= 5.0,
					dataset= "QM9",
					dataset_arg= "energy_U0",
					dataset_root= "~/data",
					derivative= True,
					distance_influence= "both",
					distributed_backend= "ddp",
					early_stopping_patience= 150,
					ema_alpha_dy= 1.0,
					ema_alpha_y= 1.0,
					embed_files= "null",
					embedding_dimension= 256,
					energy_files= "null",
					energy_weight= 1.0,
					force_files= "null",
					force_weight= 1.0,
					inference_batch_size= 128,
					load_model= "null",
					log_dir= "logs/",
					lr= 0.0004,
					lr_factor= 0.8,
					lr_min= 1.0e-07,
					lr_patience= 15,
					lr_warmup_steps= 10000,
					max_num_neighbors= 64,
					max_z= 100,
					model= "equivariant-transformer",
					neighbor_embedding= True,
					ngpus= -1,
					num_epochs= 3000,
					num_heads= 8,
					num_layers= 8,
					num_nodes= 1,
					num_rbf= 64,
					num_workers= 6,
					output_model= "Scalar",
					precision= 32,
					prior_model= None,
					rbf_type= "expnorm",
					redirect= False,
					reduce_op= "add",
					save_interval= 10,
					splits= "null",
					standardize= False,
					test_interval= 10,
					test_size= "null",
					train_size= 110000,
					trainable_rbf= False,
					val_size= 10000,
					weight_decay= 0.0
				       ),
			 "alignn": alignn.ALIGNNConfig(name="alignn"),
			"dimenet": dict(hidden_channels=128,
				    out_channels=1,
				    num_blocks=6,
				    num_bilinear=8,
				    num_spherical=7,
				    num_radial=6,
				    cutoff=5.0,
				    envelope_exponent=5,
				    num_before_skip=1,
				    num_after_skip=2,
				    num_output_layers=3,),
			 "dimenetpp": dict(hidden_channels=128,
					    out_channels=1,
					    num_blocks=4,
					    int_emb_size=64,
					    basis_emb_size=8,
					    out_emb_channels=256,
					    num_spherical=7,
					    num_radial=6,
					    cutoff=5.0,
					    max_num_neighbors=32,
					    envelope_exponent=5,
					    num_before_skip=1,
					    num_after_skip=2,
					    num_output_layers=3,)}

opt = get_parser()

""""1. DEFINE FUNCTION TO MEASURE
When using Ray, automatically DDP!"""

def get_run(opt: argparse.ArgumentParser = None):
    if opt.log:
        logger = WandbLogger(name=None, entity="argonne_gnn", project='internship')
    def run(config: dict):
        opt.amp = config["amp"] #set argparser amp options to config["amp"] 
        opt.epoches = config["num_epochs"]
        opt.batch_size = int(config["batch_size"]) #set argparser amp options to config["amp"] 
        opt.learning_rate = config["learning_rate"]
        opt.optimizer = config["optimizer"] #set argparser amp options to config["amp"] 
        print(cf.on_yellow(f"Chosen amp {opt.amp} epochs {opt.epoches} batch_size {opt.batch_size} lr {opt.learning_rate} optim {opt.optimizer}"))
	
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

        dm = DataModuleEdge(opt=opt)
        train_dataloader = dm.train_dataloader()
        valid_dataloader = dm.val_dataloader()
        test_dataloader = dm.test_dataloader()

        model = BACKBONES.get(opt.backbone, physnet.Physnet) #Uninitialized class
        model_kwargs = BACKBONE_KWARGS.get(opt.backbone, dict(dfilter=64, 
								 filter=64, 
								 cutoff=10, 
								 num_residuals=3,
								 num_residuals_atomic=2, 
								 num_interactions=5, 
								 activation_fn=torch.nn.ReLU(), 
								 dmodel=64, 
								 token_embedding_necessary=True, 
								 max_num_neighbors=32,
							         explain=opt.explain))
	
        if opt.backbone in ["schnet", "physnet", "dimenet", "dimenetpp"]:
            model = model(**model_kwargs).to(device) 
        elif opt.backbone in ["alignn"]:
            model = model(model_kwargs).to(device) #Accounting for alignn net
        elif opt.backbone in ["torchmdnet"]:
            model = torchmdnet.create_model(model_kwargs).to(device)
	
        grad_scaler = torch.cuda.amp.GradScaler(enabled=opt.amp)
	
        if config["optimizer"] == 'adam':
            optimizer = FusedAdam(model.parameters(), lr=opt.learning_rate, betas=(0.9, 0.999),
                              weight_decay=opt.weight_decay)
        elif config["optimizer"] == 'lamb':
            optimizer = FusedLAMB(model.parameters(), lr=opt.learning_rate, betas=(0.9, 0.999),
                              weight_decay=opt.weight_decay)
        elif config["optimizer"] == "sgd":
            optimizer = torch.optim.SGD(model.parameters(), lr=opt.learning_rate, momentum=0.9,
                                    weight_decay=opt.weight_decay)
        elif config["optimizer"] == "torch_adam":
            optimizer = AdamW(model.parameters(), lr=opt.learning_rate, betas=(0.9, 0.999),
                              weight_decay=opt.weight_decay)
	
        loss_func = get_loss_func
        total_training_steps = len(train_dataloader) * config["num_epochs"]
        warmup_steps = total_training_steps // opt.warm_up_split
        scheduler = get_linear_schedule_with_warmup(
		optimizer,
		num_warmup_steps=warmup_steps,
		num_training_steps=total_training_steps)
        local_rank = 0
        tmetrics = torchmetrics.MeanAbsoluteError()
	
        try:
            for epoch_idx in range(0, int(config["num_epochs"])):
                dp_train_loss = single_train(opt, model, train_dataloader, loss_func, epoch_idx, optimizer, scheduler, grad_scaler, local_rank, logger, tmetrics)
                logger.log_metrics({'DeepHyperTrainLoss': dp_train_loss}, epoch_idx) 
                torch.cuda.empty_cache()
                tmetrics.reset()
            torch.cuda.empty_cache()
            dp_val_loss = single_val(opt, model, valid_dataloader, loss_func, optimizer, scheduler, logger, tmetrics)
            tmetrics.reset()
            logger.log_metrics({'DeepHyperValLoss': dp_val_loss}, epoch_idx) 
            return -dp_val_loss #DH solves Maximum problem!

        except Exception as e:
            print(cf.red(f"Error case happend with {e}"))
            return np.finfo(float).min   

    if opt.log:
        return run, logger
    else:
        return run, None
perf_run, logger = get_run(opt=opt)

"""2. DEFINE HPPROBLEM"""
# We define a dictionnary for the default values
default_config = {
    "num_epochs": 2,
    "batch_size": 64,
    "learning_rate": 0.5,
    "optimizer": "adam",
    "amp": True,
}

##RAY DOES NOT WORK with Python 3.10.x!
# We launch the Ray run-time and execute the `run` function
# with the default configuration
# if is_gpu_available:
#     if not(ray.is_initialized()):
#         ray.init(num_cpus=n_gpus, num_gpus=n_gpus, log_to_driver=False)

#     run_default = ray.remote(num_cpus=1, num_gpus=1)(perf_run)
#     objective_default = ray.get(run_default.remote(default_config))
# else:
#     if not(ray.is_initialized()):
#         ray.init(num_cpus=1, log_to_driver=False)
#     run_default = perf_run
#     objective_default = run_default(default_config)

# print(f"Accuracy Default Configuration:  {objective_default:.3f}")

from deephyper.problem import HpProblem
import ConfigSpace as CS
import ConfigSpace.hyperparameters as CSH
cs = CS.ConfigurationSpace(seed=42)
num_epochs = CSH.UniformIntegerHyperparameter('num_epochs', lower=10, upper=100, log=False, default_value=10)
batch_size = CSH.UniformIntegerHyperparameter('batch_size', lower=128, upper=512, log=True, default_value=256)
learning_rate = CSH.UniformFloatHyperparameter('learning_rate', lower=0.0001, upper=1., log=True, default_value=0.001)
optimizer = CSH.CategoricalHyperparameter('optimizer', choices=["adam","lamb","sgd"], default_value="sgd")
amp = CSH.CategoricalHyperparameter('amp', choices=[True, False])
config_spaces = [num_epochs, batch_size, learning_rate, optimizer, amp] #List of CS

problem = HpProblem()
problem.add_hyperparameters(config_spaces)
# cs.add_hyperparameters(config_spaces) #Same as above! msg: 1/2
# problem = HpProblem(config_space=cs) #Same as above! msg: 2/2

# Add a starting point to try first
#problem.add_starting_point(**default_config)

"""3. DEFINE WHAT BACKEND TO USE"""
from deephyper.evaluator import Evaluator
from deephyper.evaluator.callback import LoggerCallback

is_gpu_available = torch.cuda.is_available()
n_gpus = torch.cuda.device_count()
def get_evaluator(run_function, method="ray"):
    # Default arguments for Ray: 1 worker and 1 worker per evaluation
    method_kwargs = {
        "num_cpus": 1,
        "num_cpus_per_task": 1,
        "callbacks": [LoggerCallback()],
	"num_workers": 2,
    }

    # If GPU devices are detected then it will create 'n_gpus' workers
    # and use 1 worker for each evaluation
    if is_gpu_available:
        method_kwargs["num_cpus"] = n_gpus
        method_kwargs["num_gpus"] = n_gpus
        method_kwargs["num_cpus_per_task"] = 1
        method_kwargs["num_gpus_per_task"] = 1
	
        print(f"Chosen {method}...")
        evaluator = Evaluator.create(
        run_function,
        method=method,
        method_kwargs=method_kwargs
        )
    print(f"Created new evaluator with {evaluator.num_workers} worker{'s' if evaluator.num_workers > 1 else ''} and config: {method_kwargs}", )
    return evaluator
evaluator = get_evaluator(perf_run, method="ray") #instance of ray/subprocess/mpi etc..

"""4. RUN SEARCH"""
from deephyper.search.hps import AMBS, CBO
search = CBO(problem, evaluator, filter_failures="ignore", multi_point_strategy="cl_max")
results = search.search(max_evals=10, ) if not opt.resume_hp else search.fit_surrogate(opt.hp_savefile) #DataFrame

"""5. Log""" 
if logger != None:
    logger.log_deephyper(results) #Log to W&B


