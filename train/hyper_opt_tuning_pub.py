import os, abc, sys
import pathlib
root = pathlib.Path(__file__).parent.parent #gpuHack directory
#sys.path.append(root) #add root directory
os.chdir(root)
import pdb
import glob
import copy
import wandb
import h5py
import random
import string
import argparse
import pandas as pd
import numpy as np
from scipy.special import softmax
from PIL import Image
from typing import *
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from curtsies import fmtfuncs as cf
import scipy.stats
import torchmetrics
import yaml
from tqdm import tqdm

from fast_ml.model_development import train_valid_test_split

from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score, accuracy_score, precision_score, recall_score
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score

import torchvision.transforms as transforms
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
import torch.nn.functional as F
import torch.optim as optim
import torch.nn as nn
import torch
import torchvision

from transformers import BertTokenizerFast as BertTokenizer, BertModel, AdamW, get_linear_schedule_with_warmup

from schnet import schnet
from physnet import physnet
from torchmdnet import torchmdnet
from crystals import cgcnn
from crystals import physnet as cphysnet
from crystals import schnet as cschnet
from crystals import torchmdnet as ctorchmdnet
from config_pub import *

import captum
  
from train.dist_utils import to_cuda, get_local_rank, init_distributed, seed_everything, \
    using_tensor_cores, increase_l2_fetch_granularity, WandbLogger
from torch.utils.data import DataLoader, DistributedSampler, Dataset
from train.training_functions_pub import train as train_molecule
from crystals.training_functions_pub import train as train_crystal
from train.dataloader import DataModuleEdge, DataModuleOthers
from crystals.dataloader import DataModuleCrystal
from train.loss_utils_pub import get_loss_func, get_loss_func_crystal

from main_pub import get_parser

""""0. DEFINE MODEL CONFIGS"""
#Called from "config_pub.py"
opt = get_parser()

""""1. DEFINE FUNCTION TO MEASURE
When using Ray, automatically DDP!"""

def get_run(opt: argparse.ArgumentParser = None):
    if opt.log:
        logger = WandbLogger(name=None, entity="argonne_gnn", project='internship')
        
    def run(config: dict):
        for key, val in config.items():
            if key == "batch_size":
                val = int(val)
            if isinstance(val, np.bool_):
                val = bool(val)
            opt.__dict__[key] = val      
        print(opt.__dict__, end=" ")
#         print(cf.on_yellow(f"Chosen amp {opt.amp} epochs {opt.epoches} batch_size {opt.batch_size} lr {opt.learning_rate} optim {opt.optimizer}"))
        print(cf.on_yellow(f"Chosen {config.items()}..."))
#         device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        device = torch.cuda.current_device()

        #Distributed Sampler Loader
        if opt.dataset in ["qm9edge"]:
    	    datamodule = DataModuleEdge(opt=opt)
        if opt.dataset in ["qm9", "md17", "ani1", "ani1x", "moleculenet"]:
    	    datamodule = DataModuleOthers(hparams=opt)
        if opt.dataset in ["cifdata","gandata"]:
    	    datamodule = DataModuleCrystal(opt=opt)
        train_loader = datamodule.train_dataloader()
        val_loader = datamodule.val_dataloader()
        test_loader = datamodule.test_dataloader()

        #Model 
        model = BACKBONES.get(opt.backbone, physnet.Physnet) #Uninitialized class
        model_kwargs = BACKBONE_KWARGS.get(opt.backbone, None) #TorchMDNet not yet!

        if opt.backbone not in ["calignn", "alignn"]: model_kwargs.update({"explain": opt.explain})  
        else: model_kwargs.explain = opt.explain #only for alignn net (due to pydantic)

        if opt.backbone in ["schnet", "physnet", "dimenet", "dimenetpp","cschnet","cphysnet","cgcnn"]:
            model = model(**model_kwargs) 
        elif opt.backbone in ["alignn","calignn"]:
            model = model(model_kwargs) #Accounting for alignn net
        elif opt.backbone in ["torchmdnet","ctorchmdnet"]:
            model = torchmdnet.create_model(model_kwargs) if opt.backbone=="torchmdnet" else ctorchmdnet.create_model(model_kwargs)
        model.to(device)
	
        grad_scaler = torch.cuda.amp.GradScaler(enabled=opt.amp)
	
        try:
#             importlib.import_module("apex.optimizers")
            from apex.optimizers import FusedAdam, FusedLAMB
        except Exception as e:
            pass

        if opt.optimizer == 'adam':
            optimizer = FusedAdam(model.parameters(), lr=opt.learning_rate, betas=(0.9, 0.999),
                                weight_decay=opt.weight_decay)
            base_optimizer = FusedAdam
            base_optimizer_arguments = dict(lr=opt.learning_rate, betas=(0.9, 0.999), weight_decay=opt.weight_decay)
        elif opt.optimizer == 'lamb':
            optimizer = FusedLAMB(model.parameters(), lr=opt.learning_rate, betas=(0.9, 0.999),
                                weight_decay=opt.weight_decay)
            base_optimizer = FusedLAMB
            base_optimizer_arguments = dict(lr=opt.learning_rate, betas=(0.9, 0.999), weight_decay=opt.weight_decay)
        elif opt.optimizer == "sgd":
            optimizer = torch.optim.SGD(model.parameters(), lr=opt.learning_rate, momentum=0.9,
                                        weight_decay=opt.weight_decay)
            base_optimizer = torch.optim.SGD
            base_optimizer_arguments = dict(lr=opt.learning_rate, momentum=0.9, weight_decay=opt.weight_decay)
        elif opt.optimizer == 'torch_adam':
            optimizer = torch.optim.Adam(model.parameters(), lr=opt.learning_rate, betas=(0.9, 0.999),
                                weight_decay=opt.weight_decay, eps=1e-8)
            base_optimizer = torch.optim.Adam
            base_optimizer_arguments = dict(lr=opt.learning_rate, betas=(0.9, 0.999), weight_decay=opt.weight_decay, eps=1e-8)
        elif opt.optimizer == 'torch_adamw':
            optimizer = torch.optim.AdamW(model.parameters(), lr=opt.learning_rate, betas=(0.9, 0.999),
                                weight_decay=opt.weight_decay, eps=1e-8)
            base_optimizer = torch.optim.AdamW
            base_optimizer_arguments = dict(lr=opt.learning_rate, betas=(0.9, 0.999), weight_decay=opt.weight_decay, eps=1e-8)
        elif opt.optimizer == 'torch_sparse_adam':
            optimizer = torch.optim.SparseAdam(model.parameters(), lr=opt.learning_rate, betas=(0.9, 0.999),
                                eps=1e-8)
            base_optimizer = torch.optim.SparseAdam
            base_optimizer_arguments = dict(lr=opt.learning_rate, betas=(0.9, 0.999), eps=1e-8)
	
        loss_func = get_loss_func
        total_training_steps = len(train_loader) * opt.epoches
        warmup_steps = total_training_steps // opt.warm_up_split
        scheduler = get_linear_schedule_with_warmup(
                                                optimizer,
                                                num_warmup_steps=warmup_steps,
                                                num_training_steps=total_training_steps)
        local_rank = get_local_rank()
        tmetrics = torchmetrics.MeanAbsoluteError()
	
        try:
            if opt.crystal:
                from crystals.training_functions_pub import load_state, save_state, single_train, single_val, single_test
            else:
                from train.training_functions_pub import load_state, save_state, single_train, single_val, single_test

            pbar = tqdm(range(0, int(config["epoches"])), total=int(config["epoches"]), unit='epoch',
                                    disable=(opt.silent or local_rank != 0))
            for epoch_idx in pbar:
                dp_train_loss, _ = single_train(opt, model, train_loader, loss_func, epoch_idx, optimizer, scheduler, grad_scaler, local_rank, logger, tmetrics)
                logger.log_metrics({'DeepHyperTrainLoss': dp_train_loss}, epoch_idx) 
                torch.cuda.empty_cache()
                tmetrics.reset()
                pbar.set_description(f"Processing epoch {epoch_idx} in local rank {local_rank}...")
            torch.cuda.empty_cache()
            dp_val_loss, _ = single_val(opt, model, val_loader, loss_func, optimizer, scheduler, logger, tmetrics)
            tmetrics.reset()
            logger.log_metrics({'DeepHyperValLoss': dp_val_loss}, epoch_idx) 
            return -dp_val_loss #DH solves Maximum problem!

        except Exception as e:
            print(cf.red(f"Error case happend with {e}"))
            return np.finfo(float).min   

    if opt.log:
        return run, logger
    else:
        return run, None
perf_run, logger = get_run(opt=opt)

"""2. DEFINE HPPROBLEM"""
# We define a dictionnary for the default values
f = open(opt.hyper_inputs, "r")
config_from_yaml = yaml.safe_load(f)

##RAY DOES NOT WORK with Python 3.10.x!
# We launch the Ray run-time and execute the `run` function
# with the default configuration
# if is_gpu_available:
#     if not(ray.is_initialized()):
#         ray.init(num_cpus=n_gpus, num_gpus=n_gpus, log_to_driver=False)

#     run_default = ray.remote(num_cpus=1, num_gpus=1)(perf_run)
#     objective_default = ray.get(run_default.remote(default_config))
# else:
#     if not(ray.is_initialized()):
#         ray.init(num_cpus=1, log_to_driver=False)
#     run_default = perf_run
#     objective_default = run_default(default_config)

# print(f"Accuracy Default Configuration:  {objective_default:.3f}")


from deephyper.problem import HpProblem
import ConfigSpace as CS
import ConfigSpace.hyperparameters as CSH

cs = CS.ConfigurationSpace(seed=42)
config_keys = config_from_yaml.keys()
config_spaces = []

for key in config_keys:
    data = config_from_yaml.get(key)
    if key == "amp":
        hp = CSH.CategoricalHyperparameter('amp', **data)
    elif key == "epoches":
        hp = CSH.UniformIntegerHyperparameter('epoches', **data)
    elif key == "batch_size":
        hp = CSH.UniformIntegerHyperparameter('batch_size', **data)
    elif key == "optimizer":
        hp = CSH.CategoricalHyperparameter('optimizer', **data)
    elif key == "learning_rate":
        hp = CSH.UniformFloatHyperparameter('learning_rate', **data)
    elif key == "gradient_clip":
        hp = CSH.UniformFloatHyperparameter('gradient_clip', **data)
    elif key == "accumulate_grad_batches":
        hp = CSH.UniformIntegerHyperparameter('accumulate_grad_batches', **data)
    elif key == "weight_decay":
        hp = CSH.UniformFloatHyperparameter('weight_decay', **data)
    config_spaces.append(hp)	

problem = HpProblem()
problem.add_hyperparameters(config_spaces)
# cs.add_hyperparameters(config_spaces) #Same as above! msg: 1/2
# problem = HpProblem(config_space=cs) #Same as above! msg: 2/2

# Add a starting point to try first
#problem.add_starting_point(**default_config)

"""3. DEFINE WHAT BACKEND TO USE"""
from deephyper.evaluator import Evaluator
from deephyper.evaluator.callback import LoggerCallback

is_gpu_available = torch.cuda.is_available()
n_gpus = torch.cuda.device_count()
def get_evaluator(run_function, method="ray"):
    # Default arguments for Ray: 1 worker and 1 worker per evaluation
    method_kwargs = {
        "num_cpus": 1,
        "num_cpus_per_task": 1,
        "callbacks": [LoggerCallback()],
	"num_workers": 2,
    }

    # If GPU devices are detected then it will create 'n_gpus' workers
    # and use 1 worker for each evaluation
    if is_gpu_available:
        method_kwargs["num_cpus"] = n_gpus
        method_kwargs["num_gpus"] = n_gpus
        method_kwargs["num_cpus_per_task"] = 1
        method_kwargs["num_gpus_per_task"] = 1
	
        print(f"Chosen {method}...")
        evaluator = Evaluator.create(
        run_function,
        method=method,
        method_kwargs=method_kwargs
        )
    print(f"Created new evaluator with {evaluator.num_workers} worker{'s' if evaluator.num_workers > 1 else ''} and config: {method_kwargs}", )
    return evaluator
evaluator = get_evaluator(perf_run, method="ray") #instance of ray/subprocess/mpi etc..

"""4. RUN SEARCH"""
from deephyper.search.hps import AMBS, CBO
search = CBO(problem, evaluator, filter_failures="ignore", multi_point_strategy="cl_max", log_dir=opt.save_hyper_dir)
results = search.search(max_evals=20, ) if not opt.resume_hp else search.fit_surrogate(os.path.join(opt.save_hyper_dir, opt.hp_savefile)) #DataFrame

"""5. Log""" 
if logger != None:
    logger.log_deephyper(results) #Log to W&B

