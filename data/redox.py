import os, abc
import pdb
import glob
import copy
import wandb
import h5py
import random
import string
import argparse
import pandas as pd
import numpy as np
from scipy.special import softmax
from PIL import Image
from typing import *
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from fast_ml.model_development import train_valid_test_split
import dgl
from curtsies import fmtfuncs as cf
import torch_geometric
import networkx as nx
import pathlib
import pickle 

from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score, accuracy_score, precision_score, recall_score
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score

import scipy.stats
import scipy.optimize

from torch.utils.data import Dataset
from torch.utils.data import DataLoader
import torch.nn.functional as F
import torch.optim as optim
import torch.nn as nn
import torch
from transformers import BertTokenizerFast as BertTokenizer, BertModel, AdamW, get_linear_schedule_with_warmup
from dgl.data import QM9EdgeDataset
import torch.distributed as dist 

import torch_geometric
from torch_geometric.data import Data, Dataset
from torch.utils.data import DataLoader, DistributedSampler
from data.smiles_parser import from_smiles
import glob
import functools
import ray

# global INVALID_COUNTER
INVALID_COUNTER = 0

@ray.remote
def parse_smiles(smile: str):
    try:
        return from_smiles(smile)
    except Exception as e:
        print(cf.on_red(f"{e} with {smile}... skipping..."))
        # INVALID_COUNTER += 1
        return None

class REDOX(Dataset):
    def __init__(self, root_dir, **kwargs):
        super().__init__()
        self.root_dir = root_dir #data directory

        data_files = os.listdir(self.root_dir) #glob.glob(os.path.join(self.root_dir, "*")) #train/val/test.csv
        data_files_idx = np.isin(data_files, ["redox_train.csv", "redox_valid.csv", "redox_test.csv"])
        data_files = np.array(data_files)[data_files_idx].tolist() #choose noly redox_train/valid/test csv files for REDOX dataset!
        
        df = pd.DataFrame(columns = ["smiles", "output"])

        import csv
        for one_file in data_files:
            one_file = os.path.join(self.root_dir, one_file)
            df = pd.concat([df, pd.read_csv(one_file, header=0).drop('n_atom', axis=1)], axis=0, ignore_index=True) #concat train-val-test 

        if pathlib.Path(os.path.join(self.root_dir, "redox_preprocessed_data.pickle")).exists():
            with open(os.path.join(self.root_dir, "redox_preprocessed_data.pickle"), "rb") as f:
                print(cf.on_green("Fetching ONLY valid SMILES and properties from pickle..."))
                data = pickle.load(f)
                self.smiles: List[Data] = data["smiles_data"] 
                self.labels: np.ndarray = data["labels"]
        else:
            with open(os.path.join(self.root_dir, "redox_preprocessed_data.pickle"), "wb") as f:
                print(cf.green("Extracing ONLY valid SMILES and properties and saving as a pickle..."))
                smiles = df.values[:,0].astype(str).tolist() #List[smiles]
                self.smiles = [parse_smiles.remote(smile) for smile in smiles]
                self.smiles: List[Union[Data, type(None)]] = ray.get(self.smiles) #List[torch_geometric.data.Data]; still returns None for invalids

                # if os.path.isfile(os.path.join(self.root_dir, "valid_redox_indices.npy")):
                if pathlib.Path(os.path.join(self.root_dir, "valid_redox_indices.npz")).exists():
                    print(cf.on_yellow("Fetching saved valid indices..."))
                    valid_indices: np.ndarray = np.load(os.path.join(self.root_dir, "valid_redox_indices.npz"))["valid_indices"] #npz compressed requires a keyword
                else:
                    valid_indices: np.ndarray = np.where(np.array(self.smiles) != None)[0] #index array (returns tuple of tuples for indexing)
                    np.savez_compressed(os.path.join(self.root_dir, "valid_redox_indices.npz"), valid_indices=valid_indices)
                    print(cf.on_blue("Saving valid indices..."))

                # print(valid_indices, len(valid_indices), len(self.smiles))
                print(f"{len(self.smiles) - len(valid_indices)} data points are removed due to failure mode...")
                # assert len(valid_indices) == len(self.smiles), "To process smiles to remove None values, valid_indices numpy array must have the same number of Boolean values"
                self.smiles: List[Data] = np.array(self.smiles)[valid_indices].tolist() #only valids...        
                self.labels: np.ndarray = df.values[:,1].astype(float)[valid_indices]
                pickle.dump(dict(smiles_data=self.smiles, labels=self.labels), f)
        
    def len(self, ):
        return len(self.labels) #valid indices

    @functools.lru_cache(maxsize=None)  # Cache loaded structures
    def get(self, idx):
        data = self.smiles[idx] #torch_geometric.data.Data #so that BatchData
        label = torch.tensor(self.labels[idx]).view(1,1) #so that (B,1)
        data.y = label ##so that (B,1)
        return data #Data(z=z, pos=pos, y=y)

if __name__ == "__main__":
    root_dir = pathlib.Path(__file__).parent #current directory
    dataset = REDOX(root_dir)
    dataset[5]
        
