# AI4MolCryst_Argonne

This repository accompanies the paper [End-to-end AI Framework for Interpretable Prediction of Molecular and Crystal Properties](https://iopscience.iop.org/article/10.1088/2632-2153/acd434)

Graph neural networks implemented in this framework include:
<br>**Small Molecules**</br>
- PhysNet: `physnet/physnet.py`
- SchNet: `schnet/schnet.py`
- MPNN/MPNN-transformer: `mpnn/mpnn.py`
- ALIGNN: `alignn/alignn.py`
- TorchMD-NET: `torchmdnet/torchmdnet.py`

<br>**Crystals/MOFs**
- CGCNN (original): `crystals/cgcnn_original.py`
- CGCNN (modified): `crystals/cgcnn.py`
- cPhysNet: `crystals/physnet.py`
- cSchNet: `crystals/schnet.py`

Code for key functionalities of the paper:
- Hyperparameter tuning (Deephyper): `train/hyper_opt_tunig_pub.py`
- Model training: `train/training_functions_pub.py`
- Interpretation methods (Saliency Maps / CAM / Grad-CAM): `train/explainer.py`

The pre-trained models and datasets are available in Zenodo [here](https://doi.org/10.5281/zenodo.7758490).

The google colab that demonstrates model infernece is available [here](https://tinyurl.com/49reb4su). The colab will continuously be adding new examples. For now, loading pretrained models for inference are available. _The updates will contain model training, hyperparameter tuning, XAI, visualizing latent space etc., as illustrated and elaborated in our paper._
