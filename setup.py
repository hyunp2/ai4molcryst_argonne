
import setuptools

setuptools.setup(
    name="ai4molcryst_argonne",
    version="0.0.1",
    author="Hyun Park",
    author_email="hyunp2@illinois.edu",
    description="Code of End-to-end AI Framework for Interpretable Prediction of Molecular and Crystal Properties",
    long_description_content_type="text/markdown",
    long_description=open('README.md').read(),
    url="git@gitlab.com:hyunp2/ai4molcryst_argonne.git",
    packages=setuptools.find_packages(),
)
